#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <getopt.h>
#include <termios.h>
#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "conv_util.h"        // a modifier le pattern
#include "header_util.h"      // a modifier le pattern
#include "cpm_modem_util.h"   // a modifier le pattern
#include "crc_util.h"         // a modifier le pattern

#define pi 3.141592653589793
#define PROGRAM_VERSION "0.0.1"

FILE *input_fid , *output_fid;

typedef struct {
				int16_t re;
				int16_t im;
			   }i16cmplx;

void print_usage()
{

	fprintf(stderr,
			"dvb2iq -%s\n\
Usage:\ndvb2iq -s SymbolRate [-i File Input] [-o File Output] [-h] \n\
-i            Input Transport stream File (default stdin) \n\
-o            OutputIQFile (default stdout) \n\
-h            help (print this help).\n\
Example : ./rx_cpfsk -i input_file -o output_file\n\
\n",
			PROGRAM_VERSION);

} /* end function print_usage */

// ===================================================================================================================================== //
// ##################################################################################################################################### //
// ===================================================================================================================================== //


int main(int argc, char **argv) 
{ 
	input_fid = stdin;
	output_fid = stdout;
	bool repeat_reading =true;
	
	double SampleRate = 0.0;

	    
    int a;
	int anyargs = 0;
	
	while (1)
	{
		a = getopt(argc, argv, "i:s:o:h:");

		if (a == -1)
		{
			if (anyargs)
				break;
			else
				a = 'h'; //print usage and exit
		}
		anyargs = 1;

		switch (a)
		{
		case 'i': // InputFile
			if ( optarg != NULL ) 
			{ 
				//fprintf(stderr,"[TX MOD] input File %s is created -- \n",optarg);
				input_fid = fopen(optarg, "r");
				if ( input_fid == NULL )
				{
					fprintf(stderr, "[TX MOD] Unable to open '%s': %s\n",optarg, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			break;
		
		case 's': // SymbolRate in KS
		SampleRate = atol(optarg)*1000;
		break;
		
		case 'o': //output file
			if ( optarg != NULL ) 
			{   
				//fprintf(stderr,"[TX MOD] Output FIFO file %s is created -- \n",optarg);
				//mkfifo(optarg,0666);
				
				output_fid = fopen(optarg, "w");
				//fprintf(stderr,"[TX MOD] Output FIFO file is created ++ \n");
				if ( output_fid == NULL )
				{
					fprintf(stderr, "[TX MOD] Unable to open '%s': %s\n",optarg, strerror(errno));
					exit(EXIT_FAILURE);
				};
			}
			break;
		case 'h': // help
		    print_usage();
		    exit(0);
		    break;
	    case -1:
		    break;	
		case '?':
			if (isprint(optopt))
			{
				fprintf(stderr, "dvb2iq `-%c'.\n", optopt);
			}
			else
			{
				fprintf(stderr, "dvb2iq: unknown option character `\\x%x'.\n", optopt);
			}
			print_usage();

			exit(1);
			break;
		default:
			print_usage();
			exit(1);
			break;
		} /* end switch a */
	}	 /* end while getopt() */
	
	if (SampleRate == 0.0) {fprintf(stderr, "[RX DEMOD] Need set a SampleRate \n"); exit(0);}
	
	bool isapipe = (fseek(input_fid, 0, SEEK_CUR) < 0); //Dirty trick to see if it is a pipe or not

	//if (isapipe)   fprintf(stderr, "[TX MOD] Using live mode\n");
	//else           fprintf(stderr, "[TX MOD] Using file mode\n");
	
	
	
   
   
	const char *crc_type = "24B";
	int crc_len  = 24;
	int *crc_bits = (int*) calloc(crc_len, sizeof(int));

	/* ====================================================================================================== */
	// MATLAB TRELLIS PARAMS //
	
		
	//    numInputSymbols: 2
	//    numOutputSymbols: 4
	//           numStates: 4
	//          nextStates: [4�2 double] : 0     0     1     1     2     2     3     3
	//             outputs: [4�2 double] : 0     3     2     1     3     0     1     2
	//          prevStates: [4�2 double] : 0     2     0     2     1     3     1     3
	//        prevStatesIn: [4�2 double] : 0     0     1     1     0     0     1     1
		
	int n_in_symb = 2;
	int n_out_symb = 4;
	int nb_states = 4;
	
	int nextStates[nb_states * n_in_symb];
	nextStates[0] = 0;   nextStates[1] = 0;   nextStates[2] = 1;   nextStates[3] = 1;   
	nextStates[4] = 2;   nextStates[5] = 2;   nextStates[6] = 3;   nextStates[7] = 3;
	
	int outputs[nb_states * n_in_symb];
	outputs[0] = 0;   outputs[1] = 3;   outputs[2] = 2;   outputs[3] = 1;   
	outputs[4] = 3;   outputs[5] = 0;   outputs[6] = 1;   outputs[7] = 2;   
	
	int prevStates[nb_states * n_in_symb];
	prevStates[0] = 0;   prevStates[1] = 2;   prevStates[2] = 0;   prevStates[3] = 2;   
	prevStates[4] = 1;   prevStates[5] = 3;   prevStates[6] = 1;   prevStates[7] = 3;
	
	int prevStatesIn[nb_states * n_in_symb];
	prevStatesIn[0] = 0;   prevStatesIn[1] = 0;   prevStatesIn[2] = 1;   prevStatesIn[3] = 1;   
	prevStatesIn[4] = 0;   prevStatesIn[5] = 0;   prevStatesIn[6] = 1;   prevStatesIn[7] = 1;
	
	/* ====================================================================================================== */
	
	// CPM parameters
	int cpm_M = 2;
	int cpm_L = 1; // filter support = L*Symbolperiod
	int cpm_K = 7;  
	int cpm_P = 10;
	int cpm_sps = 8;
	// system parameters
	double cpm_Pe_dBm = 28;   // Tx power parameters: GSM 900 MHz (33 dBm) ; GSM 1800 MHz (30 dBm) ; UMTS 2100 MHz (21 dBm) ; Wifi (20 dBm)
	double SymbolRate = SampleRate/(double)cpm_sps; 
	double cpm_BW = SymbolRate;
	// Frame parameters
	int nFFT = 128;
	int nOFDMSymbols = 8;
	int nSubFrame = 5;
	int Ng = 8;
	// channel parameters
	int Lc = 4*cpm_sps;
	int N = nFFT*cpm_sps;
	int Ng2 = Ng*cpm_sps;
	// ==========================================
	int nbBitPerSymbol = (int)log2(cpm_M);
	double CodeRate = log2(n_in_symb)/log2(n_out_symb);
		
	int nCodedSubFrameBits = nOFDMSymbols*nFFT*nbBitPerSymbol; 
	int nUncodedSubFrameBits = (int)nCodedSubFrameBits*CodeRate;     
	int nCodedFrameBits = nSubFrame * nCodedSubFrameBits;		    
	int nUncodedFrameBits = nSubFrame * nUncodedSubFrameBits;
	// ========================================== 
	int user_number = 0;
	int hdrLen = 128;
	int FrameSizeLen = 16;
	int FrameSizeMax = floor((nUncodedFrameBits-hdrLen-crc_len)/8) ;
	// ========================================== 
	int nb_bit_synchro = 32;
	int nb_symb_synchro=nb_bit_synchro/(double)nbBitPerSymbol;
	int Len_sync_signal = nb_symb_synchro*cpm_sps;
	int SamplesPerFrame = Len_sync_signal + (nSubFrame*nOFDMSymbols*(nFFT+Ng)*cpm_sps);
	// ========================================
	double Tsymb = 1/SymbolRate;
	double Tsamp = 1/SampleRate;    
	double cpm_Pe = pow(10,(cpm_Pe_dBm-30)/10);
	double sigma2_s = cpm_Pe;       
	double modIndex = (double)cpm_K/(double)cpm_P;
	double AA = (double)(cpm_M-1)/2; 
	double F_shift_cpm = -modIndex*AA/Tsymb;
	// ==========================================
	unsigned char TrafficBytes[FrameSizeMax];
	
	int *HDR                  = (int*) calloc(hdrLen, sizeof(int));	  // a modifier le type  
	int *dataBits             = (int*) calloc(nUncodedFrameBits-crc_len, sizeof(int)); // a modifier le type
	int *UncodedFrameBits     = (int*) calloc(nUncodedFrameBits, sizeof(int)); // a modifier le type
	int *CodedFrameBits       = (int*) calloc(nCodedFrameBits, sizeof(int));   // a modifier le type
	int *ICodedFrameBits       = (int*) calloc(nCodedFrameBits, sizeof(int));   // a modifier le type
	int *UncodedSubFrameBits  = (int*) calloc(nUncodedSubFrameBits, sizeof(int)); // a modifier le type
	int *CodedSubFrameBits    = (int *) calloc (nCodedSubFrameBits, sizeof(int)); // output data vector
	
	double* Tx_cpe_signal_data_OFDM_re = (double *) calloc ( nFFT*nbBitPerSymbol*cpm_sps , sizeof(double));
    double* Tx_cpe_signal_data_OFDM_im = (double *) calloc ( nFFT*nbBitPerSymbol*cpm_sps , sizeof(double));
	
	int *mcodedBitsOFDM = (int*) calloc(nFFT*nbBitPerSymbol, sizeof(int)); // a modifier le type
	
	double *Tx_baseband_signal_sync_re = (double*) calloc(Len_sync_signal, sizeof(double));
	double *Tx_baseband_signal_sync_im = (double*) calloc(Len_sync_signal, sizeof(double));
	
	double *Tx_baseband_signal_data_re = (double*) calloc(nSubFrame*nOFDMSymbols*nFFT*cpm_sps, sizeof(double));
	double *Tx_baseband_signal_data_im = (double*) calloc(nSubFrame*nOFDMSymbols*nFFT*cpm_sps, sizeof(double));
   
	double *Tx_baseband_signal_Data_re = (double*) calloc(nSubFrame*nOFDMSymbols*(nFFT+Ng)*cpm_sps, sizeof(double));
	double *Tx_baseband_signal_Data_im = (double*) calloc(nSubFrame*nOFDMSymbols*(nFFT+Ng)*cpm_sps, sizeof(double));
	
	
	
	i16cmplx *Tx_baseband_signal=(i16cmplx *)malloc(SamplesPerFrame*sizeof(i16cmplx)); 
	
	/* Auxiliary variables */
	int subframe,symb,index,i,j,k,n;
	double temp_re, temp_im; 
	double sum_value_re,sum_value_im,mean_value_re,mean_value_im;
	// ******************************************************************************************************
	int bits_synchro[nb_bit_synchro];
	bits_synchro[0]  = 0;  bits_synchro[1]  = 0;  bits_synchro[2]  = 0;  bits_synchro[3]  = 1;  bits_synchro[4]  = 1;  bits_synchro[5]  = 0;
	bits_synchro[6]  = 1;  bits_synchro[7]  = 0;  bits_synchro[8]  = 1;  bits_synchro[9]  = 1;  bits_synchro[10] = 0;  bits_synchro[11] = 0;
	bits_synchro[12] = 1;  bits_synchro[13] = 1;  bits_synchro[14] = 1;  bits_synchro[15] = 1;  bits_synchro[16] = 1;  bits_synchro[17] = 1;
	bits_synchro[18] = 1;  bits_synchro[19] = 1;  bits_synchro[20] = 1;  bits_synchro[21] = 1;  bits_synchro[22] = 0;  bits_synchro[23] = 0;
	bits_synchro[24] = 0;  bits_synchro[25] = 0;  bits_synchro[26] = 0;  bits_synchro[27] = 1;  bits_synchro[28] = 1;  bits_synchro[29] = 1;
	bits_synchro[30] = 0;  bits_synchro[31] = 1;  // %1ACFFC1D
	// Not operator
	for(n=0;n<nb_bit_synchro;n++){bits_synchro[n]= 1-bits_synchro[n];}
	// CPE	
	double* Tx_cpe_signal_sync_re = (double *) calloc ( nb_symb_synchro*cpm_sps , sizeof(double));
    double* Tx_cpe_signal_sync_im = (double *) calloc ( nb_symb_synchro*cpm_sps , sizeof(double));
    
	CPFSK_mod(bits_synchro,nb_symb_synchro,Tsymb,cpm_sps,modIndex,cpm_Pe, Tx_cpe_signal_sync_re, Tx_cpe_signal_sync_im); // a modifier

	for(n=0;n<Len_sync_signal;n++)
	{
	   temp_re = (Tx_cpe_signal_sync_re[n]*cos(2*pi*F_shift_cpm*n*Tsamp)) - (Tx_cpe_signal_sync_im[n]*sin(2*pi*F_shift_cpm*n*Tsamp));
	   temp_im = (Tx_cpe_signal_sync_re[n]*sin(2*pi*F_shift_cpm*n*Tsamp)) + (Tx_cpe_signal_sync_im[n]*cos(2*pi*F_shift_cpm*n*Tsamp));
	   
	   Tx_baseband_signal_sync_re[n] = temp_re;
	   Tx_baseband_signal_sync_im[n] = temp_im;
	}
	// ******************************************************************************************************
	const char *permut_filename,*input_filename,*output_filename;
	
	permut_filename = "Permut_CPFSK_8_128.bin";
	int16_t *permut= (int16_t*) calloc(nCodedSubFrameBits, sizeof(int16_t));
	FILE *permut_fid = stdin;
	if ( permut_filename != NULL ) {
		permut_fid = fopen(permut_filename, "rb" );
		if ( permut_fid == NULL ) {
			perror("");
			return 1;
		}
	}
	
	//for(i=0;i<nCodedSubFrameBits;i++) { fprintf(stderr,"[TX MOD] Permut[%i] = %i \n",i,permut[i]);}
	
	int nbRead = fread(permut, sizeof(int16_t),nCodedSubFrameBits,permut_fid);
	if (nbRead != nCodedSubFrameBits) fprintf(stderr,"[TX MOD] Error Reading PR file: nbRead = %i  nCodedSubFrameBits =% i  \n",nbRead,nCodedSubFrameBits);
	fclose(permut_fid);
	
	
	clock_t temps1,temps2;
    double Elapsed;
    double Elapsed_ref = SamplesPerFrame/SampleRate; // frame duration in seconds
    unsigned int uint_Elapsed_ref = (unsigned int)((SamplesPerFrame/SampleRate)*CLOCKS_PER_SEC); // frame duration in seconds (uint)
    unsigned int uint_Elapsed;

	/* ============================================================================================================================= */ 
	/* ################################################### Starting Tx processing ##################################################
	/* ============================================================================================================================= */
	int issleep;
	
	int count, FrameSize;
	bool valid_to_write = true;
	
	bool valid_CPE_demod,valid_CC_decode,valid_frame_detect;
	
	double Tx_Throughput;
	int FrameCount = 0;
    static int debugCnt = 0;
    
	while (valid_to_write)
	{
		temps1 = clock();
		// ############################################   Select Frame  #########################################"
		
		FrameSize = fread(TrafficBytes,sizeof(unsigned char),FrameSizeMax,input_fid);
		if ((FrameSize<=0) || (FrameSize>FrameSizeMax))
		{
		   if (repeat_reading) 
		   {
			 fseek(input_fid,0, SEEK_SET);
			 FrameCount = 0;
			}
	       else
	       {
	         fprintf(stderr,"[TX MOD] No more samples to Read, quit \n");
	         break;
	       }
			
		}
		else
		{ 							
			// #######################################   Remove DC component  ########################################"
			// generate HDR
			generate_HDR(hdrLen,user_number, FrameCount,FrameSize,HDR); // function 1****
			// construct Uncode Frame bits without CRC
			for(i=0;i<nUncodedFrameBits-crc_len;i++)
			{
			  if (i %2 == 0)  dataBits[i] = 0;
			  else            dataBits[i] = 1;
			  
			  if (i<hdrLen)                                       dataBits[i] = HDR[i];
			  if (((i-hdrLen)>=0) && (i-hdrLen)<(8*FrameSize)) 
			  {
				  index = (i-hdrLen)/8;
				  k = (i-hdrLen)%8;
				  dataBits[i] = ((TrafficBytes[index] & (0x01 << k)) >> k);
			  }
			}
			
			// Calculate CRC
			
			lte_crc_cal(dataBits,
						nUncodedFrameBits-crc_len,
						crc_type,
						crc_bits); // 
					  
			// ADD CRC
			
			for(i=0;i<nUncodedFrameBits;i++)
			{ 
			  if (i<(nUncodedFrameBits-crc_len))   UncodedFrameBits[i] = dataBits[i];
			  else                                 UncodedFrameBits[i] = crc_bits[i-(nUncodedFrameBits-crc_len)];
			}
			// ######################################### Convolutional Encoder #########################################"
			for(subframe=0;subframe<nSubFrame;subframe++)
			{      
				// Select SubFrame Uncoded Bits
				for(i=0;i<nUncodedSubFrameBits;i++){ UncodedSubFrameBits[i] = UncodedFrameBits[subframe+i*nSubFrame];}
				// Convolutive encoder
				Conv_Encoder( UncodedSubFrameBits,
							  nUncodedSubFrameBits,
							  n_in_symb,
							  n_out_symb,
							  nb_states,
							  nextStates,
							  outputs,
							  CodedSubFrameBits);
				// Add SubFrame Coded Bits
				for(n=0;n<nCodedSubFrameBits;n++){ICodedFrameBits[subframe*nCodedSubFrameBits+n]= CodedSubFrameBits[permut[n]-1];}
				
			}
			// ############################################# CPE coding ############################################"
			
			for(subframe=0;subframe<nSubFrame;subframe++){ 
				
				for(symb=0;symb<nOFDMSymbols;symb++)
				{  
				   // Not operator      
				   for(n=0;n<nFFT*nbBitPerSymbol;n++)
				   { 
					  mcodedBitsOFDM[n] = 1-ICodedFrameBits[subframe*(nOFDMSymbols*nFFT*nbBitPerSymbol)+symb*nFFT*nbBitPerSymbol+n]; 
				   }
				
				   CPFSK_mod(mcodedBitsOFDM,nFFT*nbBitPerSymbol,Tsymb,cpm_sps,modIndex,cpm_Pe, Tx_cpe_signal_data_OFDM_re, Tx_cpe_signal_data_OFDM_im);
				   for(n=0;n<N;n++) 
					{ 
						index = subframe*(nOFDMSymbols*N)+symb*N+n;  					
						Tx_baseband_signal_data_re[index] = (Tx_cpe_signal_data_OFDM_re[n]*cos(2*pi*F_shift_cpm*index*Tsamp)) - (Tx_cpe_signal_data_OFDM_im[n]*sin(2*pi*F_shift_cpm*index*Tsamp));
						Tx_baseband_signal_data_im[index] = (Tx_cpe_signal_data_OFDM_re[n]*sin(2*pi*F_shift_cpm*index*Tsamp)) + (Tx_cpe_signal_data_OFDM_im[n]*cos(2*pi*F_shift_cpm*index*Tsamp));
						
					}
				}
			}
			// ########################################## ADD Cyclic Prefix #######################################"
				
			//fprintf(stderr,"[TX MOD] ADD Cyclic Prefix .................... >> ");
			//double sum_value_re,sum_value_im,mean_value_re,mean_value_im;
			sum_value_re = 0.0;
			sum_value_im = 0.0;
			
			for(k=0; k<nSubFrame; k++)
			{   
			   for(j=0; j<nOFDMSymbols; j++) 
			   {      
				  for(i=0; i<(N+Ng2); i++) 
				  {
					index = k*(nOFDMSymbols*(N+Ng2))+j*(N+Ng2)+i;
					if (i<Ng2) 
					  {
					   Tx_baseband_signal_Data_re[index]= Tx_baseband_signal_data_re[k*(nOFDMSymbols*N)+j*N+(N-Ng2+i)];
					   Tx_baseband_signal_Data_im[index]= Tx_baseband_signal_data_im[k*(nOFDMSymbols*N)+j*N+(N-Ng2+i)];
					  }
					else 
					  {
					   Tx_baseband_signal_Data_re[index]= Tx_baseband_signal_data_re[k*(nOFDMSymbols*N)+j*N+(i-Ng2)];
					   Tx_baseband_signal_Data_im[index]= Tx_baseband_signal_data_im[k*(nOFDMSymbols*N)+j*N+(i-Ng2)];
					  } 
					  sum_value_re += Tx_baseband_signal_Data_re[index];
					  sum_value_im += Tx_baseband_signal_Data_im[index]; 
				  }
			   }
			}
			mean_value_re = sum_value_re/(double)(nSubFrame*nOFDMSymbols*(N+Ng2));
			mean_value_im = sum_value_im/(double)(nSubFrame*nOFDMSymbols*(N+Ng2));
			
			//fprintf(stderr,"[TX MOD] Cyclic Prefix. \n");
			
			// ######################################### Signal calibration #######################################"
			
			for (n=0;n<(nSubFrame*nOFDMSymbols*(N+Ng2));n++)
			{
				Tx_baseband_signal_Data_re[n] = Tx_baseband_signal_Data_re[n]-mean_value_re;
				Tx_baseband_signal_Data_im[n] = Tx_baseband_signal_Data_im[n]-mean_value_im;
			} 
			// ############################ ADD synchro signal/ ref signal / UW signal ############################"
			
			n = 0;
			while ((n<SamplesPerFrame)&& valid_to_write)
			{   
				if (n<Len_sync_signal)
				{
					if ((fabs(Tx_baseband_signal_sync_re[n])>=1) || (fabs(Tx_baseband_signal_sync_im[n])>=1)) valid_to_write = false;
					
					Tx_baseband_signal[n].re = (int16_t)floor(Tx_baseband_signal_sync_re[n]*32767.0);  // float
					Tx_baseband_signal[n].im = (int16_t)floor(Tx_baseband_signal_sync_im[n]*32767.0);  // float
				}
				else
				{
					if ((fabs(Tx_baseband_signal_Data_re[n-Len_sync_signal])>=1) || (fabs(Tx_baseband_signal_Data_im[n-Len_sync_signal])>=1)) valid_to_write = false;
					
					Tx_baseband_signal[n].re = (int16_t)floor(Tx_baseband_signal_Data_re[n-Len_sync_signal]*32767.0);  // float
					Tx_baseband_signal[n].im = (int16_t)floor(Tx_baseband_signal_Data_im[n-Len_sync_signal]*32767.0);  // float
				}
								
				n++;
			}
			
			temps2 = clock();
			Elapsed = ((double) (temps2 - temps1)) / CLOCKS_PER_SEC;
			uint_Elapsed = (temps2 - temps1);
			issleep = 0;
			if (uint_Elapsed<=uint_Elapsed_ref-60) 
			{
			  issleep = 1;
			  usleep((unsigned int)floor((Elapsed_ref-Elapsed)*1000000.0));
			  while (uint_Elapsed<=uint_Elapsed_ref-60)
			  {
				 temps2 = clock(); 
				 uint_Elapsed = (temps2 - temps1);
			  }
		    }
			
			
					
			// ############################################ Write Data ############################################"
			if (valid_to_write)
			{
				temps2 = clock();
			    Elapsed = ((double) (temps2 - temps1)) / CLOCKS_PER_SEC;			    
			    Tx_Throughput = (SamplesPerFrame/Elapsed)/1000; // [KB/s] 
			    
				fprintf(stderr,"\n[TX MOD] [%d] : valide write -- numFrame = %d -- issleep = %d -- Tx_Throughput= %2.3f [KB/s].\n",debugCnt,FrameCount,issleep,Tx_Throughput);
				count = fwrite(Tx_baseband_signal,sizeof(i16cmplx),SamplesPerFrame,output_fid);
				if (count<SamplesPerFrame)
				{
					fprintf(stderr,"[TX MOD] No more samples to write, quit \n");
					break;
				} 
				//fprintf(stderr,"\n[TX MOD] writing ++ < %i > ........ \n",FrameCount+1);
			}
		   FrameCount++;  
		   debugCnt++ ; 
	   }     
	}
	fclose(input_fid);
	fclose(output_fid);
	//fprintf(stderr,"\n[TX MOD] End of Tx ......\n");
	
	/* ============================================================================================================================= */ 
	/* #################################################### Ending Tx processing ###################################################
	/* ============================================================================================================================= */
	//fprintf(stderr,"[TX MOD] nBytes =%i",sizeof(int16_t));
	//fprintf(stderr,"[TX MOD] SamplesPerFrame =%i \n",SamplesPerFrame);
	// lib�rer la m�moire
	
	
	
	free(HDR);
	free(UncodedFrameBits);
	free(CodedFrameBits);
	free(UncodedSubFrameBits);
	free(CodedSubFrameBits);
	free(ICodedFrameBits);
	free(permut);	
	free(Tx_baseband_signal_sync_re);
	free(Tx_baseband_signal_sync_im);
	free(mcodedBitsOFDM);
	free(Tx_baseband_signal_data_re);
	free(Tx_baseband_signal_data_im);
	free(Tx_cpe_signal_data_OFDM_re);
	free(Tx_cpe_signal_data_OFDM_im);
	free(Tx_cpe_signal_sync_re);
	free(Tx_cpe_signal_sync_im);
	free(Tx_baseband_signal_Data_re);
	free(Tx_baseband_signal_Data_im);
	free(Tx_baseband_signal);
	
	//free(bits_synchro);
	//free(TrafficBytes);
   return 0;
}
	
