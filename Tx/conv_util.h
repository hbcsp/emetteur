#include "math.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <stdbool.h>
//#include <stddef.h>
//#include <errno.h>
//#include <string.h>
//#include <time.h>
//#include <unistd.h>
//#include <math.h>
//#include <windows.h>

#define SQUARE(x) ((x)*(x))
#ifndef max
	#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef getBitValue
	# define getBitValue( n, val) ((val & (0x01 << n)) >> n)
#endif

/* ============================================================================================================*/

void Conv_Encoder( int *UncodedBits,
                   int nUncodedBits,
                   int n_in_symb,
                   int n_out_symb,
                   int nb_states,
                   int *nextStates,
                   int *outputs,
				   int *CodedBits);

double max_star_c(double x,double y);

void Conv_BCJR_Decoder( double *llr_codes,
                        int nCodedBits,
                        int n_in_symb,
                        int n_out_symb,
                        int nb_states,
                        int *nextStates,
                        int *outputs,
                        int *prevStates,
                        int *prevStatesIn,
                        double *APP_CodedBits,
						int *Est_UncodedBits);
						
/* ============================================================================================================*/

void Conv_Encoder( int *UncodedBits,
                   int nUncodedBits,
                   int n_in_symb,
                   int n_out_symb,
                   int nb_states,
                   int *nextStates,
                   int *outputs,
				   int *CodedBits)
{

	/* Auxiliary variables */
	int i_sym = 0; // input symbol value
	int i_sym_cnt = 0; // input symbol count
	int o_sym = 0; // output symbol value
	int bit_cnt = 0;
	
	

	
	// INPUTS PROCESSING
    int i_sym_bsz = (int)( log(n_in_symb) / log(2) ); // bit size of input symbol
    int o_sym_bsz = (int)( log(n_out_symb) / log(2) ); // bit size of output symbol	
	int nCodedBits = (int)(o_sym_bsz * (nUncodedBits / i_sym_bsz)); // bit size  of output data vector

	// MATLAB OUTPUTS 
    
	// FUNCTION PROCESSING
   
    int current_state = 0; // first state value - should be able different from 0

	// For each complete symbol at input
	//      Complete : because of the truncature due to casting and/or int divide see also nCodedBits
    for( i_sym_cnt= 0; i_sym_cnt< (int)( nUncodedBits/ i_sym_bsz); i_sym_cnt++){

		// translate input bit data vector into symbol value
		i_sym = 0;
		for( bit_cnt= 0; bit_cnt< i_sym_bsz; bit_cnt++ ){
			i_sym = i_sym << 1; // offset MSB before add LSB
			i_sym += (int)UncodedBits[ bit_cnt + (i_sym_cnt * i_sym_bsz)]; // should be binary
		}
        
		// define from input_symbol the output_symbol and translates it into output_data_vector
		//
		// current_state equivalent to row in Matlab trellis.outputs
		// nb_states equivalent to col in Matlab trellis.outputs
		o_sym = (int)outputs[ current_state + (i_sym * nb_states) ];
		for( bit_cnt= 0; bit_cnt< o_sym_bsz; bit_cnt++) {
			CodedBits[ bit_cnt + (i_sym_cnt * o_sym_bsz)] = getBitValue( o_sym_bsz-bit_cnt-1, o_sym);
		}
		
		current_state = (int)nextStates[current_state + (i_sym * nb_states)]; 
    } 
	
} 



double max_star_c(double x,double y){
    return  max(x,y)+log(1+exp(-fabs(x - y)));
}





void Conv_BCJR_Decoder( double *llr_codes,
                        int nCodedBits,
                        int n_in_symb,
                        int n_out_symb,
                        int nb_states,
                        int *nextStates,
                        int *outputs,
                        int *prevStates,
                        int *prevStatesIn,
                        double *APP_CodedBits,
						int *Est_UncodedBits)
{
						
    						
    
     /* Auxiliary variables */
    int i_aux, j_aux, k_aux,init_state,init_symb,nxt_state,bb,state,input_,indx,indx2,ii,symb,jj,bit_pos;
    double aux_prob,log_prob;    
    /* Equalization variables */
    
    double norm_alpha,norm_beta, sum_alpha,sum_beta,sum_,norm_pr,norm_pr_out,sum_0,sum_1; 
    
    // ============================================
    
    
    int log2_M_in  = floor(log((double)n_in_symb)/log(2));
    int log2_M_out = floor(log((double)n_out_symb)/log(2));
    int L_rx       = floor(nCodedBits/log2_M_out) ;
    
    int mapping_in[n_in_symb];
    for(i_aux=0;i_aux<n_in_symb;i_aux++){
        mapping_in[i_aux]=i_aux; 
    }
    
    int mapping_out[n_out_symb];
    for(i_aux=0;i_aux<n_out_symb;i_aux++){
        mapping_out[i_aux]=i_aux; 
    }

    
    // OUTPUTS   
	
	double *log_P_in   = (double *) calloc (n_in_symb*L_rx , sizeof(double));
    double *log_P_out  = (double *) calloc (n_out_symb*L_rx , sizeof(double));
	
    
    // FUNCTION PROCESSING
    
        /*======================Computing Log Prob(symbol_out) by bit LLR =====================================*/
        

        double *log_prob_symb_out=(double *) calloc(n_out_symb*L_rx , sizeof(double));
        for (i_aux=0;i_aux<n_out_symb;i_aux++){
            for (j_aux=0;j_aux<L_rx;j_aux ++){
                aux_prob = 0;
                for (k_aux=0;k_aux<log2_M_out;k_aux++){
                    aux_prob = aux_prob + 0.5*(1-2*getBitValue((log2_M_out-1)-k_aux,mapping_out[i_aux]))*llr_codes[(j_aux*log2_M_out)+k_aux];
                }
                log_prob_symb_out[i_aux+j_aux*n_out_symb] = aux_prob;
             }

        }
        /*====================== Computing gamma_indx (s,s') for all s leading to s' =====================================*/
         
        double *log_gamma=(double *) calloc(nb_states*n_in_symb*L_rx , sizeof(double));

        for(j_aux=0;j_aux<L_rx;j_aux++){

            /*Computing gamma_indx (s,s') for all s leading to s'*/
            for (state=0;state<nb_states;state++){
                for (input_ = 0;input_<n_in_symb;input_++){
                    symb=(int)outputs[state +input_*nb_states];
                   log_gamma[state+(nb_states)*input_+j_aux*(nb_states*n_in_symb)] = log_prob_symb_out[symb+(j_aux*n_out_symb)]; 
                 }
            }
         }
         
        /*====================== Computing alphas and betas =====================================*/

          /* Initializing aphas betas and gammas */
         double *log_alpha = (double *) calloc(nb_states*(L_rx+1) , sizeof(double)); 
         double *log_beta  = (double *) calloc(nb_states*(L_rx+1) , sizeof(double)); 

        for (j_aux=0;j_aux<L_rx+1;j_aux++){
            for(i_aux=0;i_aux<nb_states;i_aux++){
                log_alpha[i_aux + j_aux*nb_states] = -1e9;
                log_beta[i_aux + j_aux*nb_states] = -1e9;
            }
        }

         for(i_aux=0;i_aux<nb_states;i_aux++){
            log_beta[(nb_states)*(L_rx)+ i_aux] =log(1/(double)nb_states);
            /*log_alpha[i_aux] =log(1/(double)nb_states);*/
        }   
       log_alpha[0]=0;

        /* Computing alphas and betas*/ 

        for(indx=0;indx<L_rx-1;indx++){
            indx2=L_rx-indx-1;

                norm_alpha = -1e9;
                norm_beta = -1e9;
                for(state=0; state<nb_states; state++){
                            sum_alpha = -1e9;
                             for (ii=0; ii<n_in_symb; ii++){
                               sum_alpha =max_star_c(sum_alpha,log_gamma[((int)prevStates[state +ii*nb_states]+ (nb_states*(int)prevStatesIn[state +ii*nb_states])+(indx*nb_states*n_in_symb))]+log_alpha[(int)prevStates[state +(ii*nb_states)]+(nb_states*indx)]);
                            }
                             log_alpha[state+((indx+1)*nb_states)]= sum_alpha;
                            norm_alpha = max_star_c(norm_alpha,sum_alpha);
                             /*----------------------------------------------------*/
                            sum_beta = -1e9;
                            for (ii=0; ii<n_in_symb; ii++){
                                sum_beta= max_star_c(sum_beta, log_gamma[state +ii*(nb_states)+indx2*(nb_states*n_in_symb)]+log_beta[(int)nextStates[state +ii*nb_states]+(indx2+1)*(nb_states)]);
                            }
                            log_beta[state+indx2*nb_states]=sum_beta;
                            norm_beta = max_star_c(norm_beta, sum_beta);

                 }

                for(state=0; state<nb_states; state++){
                    log_alpha[state +(indx+1)*nb_states] =  log_alpha[state +(indx+1)*nb_states]-norm_alpha;
                    log_beta[state +(indx2*nb_states)] = log_beta[state +(indx2*nb_states)]-norm_beta;
                 }
         }

         // calculate probablilities
       
         for(indx=0;indx<L_rx;indx++){
             /*================================================================*/
               norm_pr = -1e9;
                for (symb=0; symb<n_in_symb;symb++){
                    sum_ =-1e9; 
                    for (ii=0; ii<nb_states; ii++){
                        sum_ = max_star_c(sum_,(log_alpha[ii +(indx*nb_states)]+log_gamma[ii+(symb*nb_states)+(indx*nb_states*n_in_symb)]+log_beta[(int)nextStates[ii+(symb*nb_states)]+(indx+1)*nb_states]));
                    }
                    log_P_in[symb +indx*n_in_symb]=sum_;
                    norm_pr = max_star_c(norm_pr,log_P_in[symb +indx*n_in_symb]);                    
                }
                for(symb=0; symb<n_in_symb; symb++){
                   log_P_in[symb+indx*n_in_symb] = log_P_in[symb+indx*n_in_symb]-norm_pr;
                 }
              /*================================================================*/
               norm_pr = -1e9;
                for (symb=0; symb<n_out_symb;symb++){
                    sum_ =-1e9; 
                    for (jj=0; jj<n_in_symb; jj++){
                        for (ii=0; ii<nb_states; ii++){
                            if (symb==(int)outputs[ii +jj*nb_states]){         
                               nxt_state=(int)nextStates[ii+jj*nb_states];   
                               sum_ = max_star_c(sum_,(log_alpha[ii+(indx*nb_states)]+log_gamma[ii+(jj*nb_states)+(indx*nb_states*n_in_symb)]+log_beta[nxt_state+(indx+1)*nb_states]));
                            }
                        }
                    }
                  log_P_out[symb +indx*n_out_symb]=sum_;
                  norm_pr = max_star_c(norm_pr,log_P_out[symb +indx*n_out_symb]);
                }
                for(symb=0; symb<n_out_symb; symb++){
                   log_P_out[symb+indx*n_out_symb] = log_P_out[symb+indx*n_out_symb]-norm_pr;
                }  
				         
               /*================================================================*/

               for (bit_pos=0; bit_pos<log2_M_in;bit_pos++){
                    sum_0 = -1e9;
                    sum_1 = -1e9; 
                    for (ii=0;ii<n_in_symb;ii++){
                        if (getBitValue(bit_pos,mapping_in[ii])==0){
                            sum_0 =  max_star_c(sum_0,log_P_in[mapping_in[ii]+indx*n_in_symb]);
                            }
                        else if (getBitValue(bit_pos,mapping_in[ii])==1){
                            sum_1 =  max_star_c(sum_1,log_P_in[mapping_in[ii]+indx*n_in_symb]);
                        }
                    }
                    
                    if (sum_1> sum_0)  Est_UncodedBits[log2_M_in-1-bit_pos+(indx*log2_M_in)] = 1;
                    else               Est_UncodedBits[log2_M_in-1-bit_pos+(indx*log2_M_in)] = 0;
                    
                }
               /*================================================================*/
               for (bit_pos=0; bit_pos<log2_M_out;bit_pos++){
                    sum_0 = -1e9;
                    sum_1 = -1e9; 
                    for (ii=0;ii<n_out_symb;ii++){
                        if (getBitValue(bit_pos,mapping_out[ii])==0){
                            sum_0 =  max_star_c(sum_0,log_P_out[mapping_out[ii]+indx*n_out_symb]);
                            }
                        else if (getBitValue(bit_pos,mapping_out[ii])==1){
                            sum_1 =  max_star_c(sum_1,log_P_out[mapping_out[ii]+indx*n_out_symb]);
                        }
                    }
                    APP_CodedBits[log2_M_out-1-bit_pos+(indx*log2_M_out)] = sum_0 - sum_1;                     
                }
               /*================================================================*/              
        }
        
       

    free(log_P_in);
    free(log_P_out);
    free(log_prob_symb_out);
    free(log_gamma);
    free(log_alpha);
    free(log_beta);
        
    
} 

