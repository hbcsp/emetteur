#include<stdio.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <math.h>
#include <string.h>
/*#define  pi  M_PI*/
#define pi 3.141592653589793

void CPFSK_mod(int *InputBits, int nInputBits, double Tsymb, int cpm_sps, double modIndex, double cpm_Pe, double *cpe_signal_re, double *cpe_signal_im);


double log2(double x );

double bessi0(double x );

void Multisymb_detection_function(int M, int symb_block_size, double *set_symb_block, double *symb_combination, double *log_prob_symb, double *signal_received_block_re, double *signal_received_block_im, double h,bool channel_coherent,double *prob_symb, double *Z, double a, double Es, double N0, double phase_shift_begin_block, int nb_combination);

void Multisymb_decision_function(int M, int fin, int symb_block_size, double *binary_combination, double* prob_symb, double *prob_binary, int debut);

void Multisymb_detection_decision_c2_milieu_block( double *symb_frame,
                                                   double *symb_combination,
                                                   double *received_signal_matched_re,
                                                   double *received_signal_matched_im,
                                                   double h,
                                                   double *a,
                                                   double N0, 
                                                   double Es,
                                                   double *log_prob_symb,
                                                   bool channel_coherent,
                                                   int M,
                                                   int symb_block_size,
                                                   double *binary_combination,
                                                   int symb_frame_size, 
                                                   bool sliding,
												   double *APP_bit_frame,
												   double *Z_tab);
												   
/* ======================================================================================================================= */
/* ####################################################################################################################### */												   
/* ======================================================================================================================= */												   
												   
												   
void CPFSK_mod(int *InputBits, int nInputBits, double Tsymb, int cpm_sps, double modIndex, double cpm_Pe, double *cpe_signal_re, double *cpe_signal_im)
        
{        
        
   int Un, ii,jj;
   double Tsamp = Tsymb/(double)cpm_sps;
   double phase;
   
   double* q_t = (double *) calloc (cpm_sps , sizeof(double));
   for (jj=0;jj<cpm_sps;jj++){
      q_t[jj] = jj*Tsamp/(2*Tsymb);
   }

   
   int Vn=0;
   for (ii=0;ii<nInputBits;ii++){
       
       Un = InputBits[ii];
       
       for (jj=0;jj<cpm_sps;jj++)
		{
            phase = 2*pi*modIndex*( 2*Un*q_t[jj] + Vn); 
            cpe_signal_re[ii*cpm_sps + jj] = sqrt(cpm_Pe)*cos(phase);
            cpe_signal_im[ii*cpm_sps + jj] = sqrt(cpm_Pe)*sin(phase);
            
            //printf("\n n = %i  value = %2.3f \n",jj,modIndex);
        }
        // update initial phase
        Vn=Vn+Un;
   }
   
  
  free(q_t);

}     												   
												   
/* ===============================================================================================================*/
double log2( double x )
{
    double y;
    
    y=log(x)/log(2);
    return y;
}
/* ===============================================================================================================*/
/*------------------------------------------------------------*/
/* PURPOSE: Evaluate modified Bessel function In(x) and n=0.  */
/*------------------------------------------------------------*/
double bessi0( double x )
{
    double ax,ans;
    double y;
    
    
    if ((ax=fabs(x)) < 3.75) {
        y=x/3.75,y=y*y;
        ans=1.0+y*(3.5156229+y*(3.0899424+y*(1.2067492
                +y*(0.2659732+y*(0.360768e-1+y*0.45813e-2)))));
    } else {
        y=3.75/ax;
        ans=(exp(ax)/sqrt(ax))*(0.39894228+y*(0.1328592e-1
                +y*(0.225319e-2+y*(-0.157565e-2+y*(0.916281e-2
                +y*(-0.2057706e-1+y*(0.2635537e-1+y*(-0.1647633e-1
                +y*0.392377e-2))))))));
    }
    return ans;
}
/* ===============================================================================================================*/
/*Fonction calculant prob_symb et Z*/
void Multisymb_detection_function(int M, int symb_block_size, double *set_symb_block, double *symb_combination, double *log_prob_symb, double *signal_received_block_re, double *signal_received_block_im, double h,bool channel_coherent,double *prob_symb, double *Z, double a, double Es, double N0, double phase_shift_begin_block, int nb_combination)
{
    
    double* mu_symb_combination_re = (double *) calloc ( nb_combination , sizeof(double));
    double* mu_symb_combination_im = (double *) calloc ( nb_combination , sizeof(double));
    double* log_prob_symb_block = (double *) calloc ( nb_combination , sizeof(double));
    int k, index, counter, j, jj;
    int sum_symb_combination=0;
    double sum_prob_symb=0;
    double sum_prob_symb2=0;
    double X=0;
    for (k=0;k<nb_combination;k++)
    {
        counter=0;
        for (j=0;j<symb_block_size;j++)
        {
            if (set_symb_block[j] == symb_combination[k+j*nb_combination])
            {
                counter=counter+1;
            }
        }
        if (counter==symb_block_size)
        {
            index=k;
            
        }
        mu_symb_combination_re[k]=signal_received_block_re[((int) symb_combination[k])*symb_block_size];
        mu_symb_combination_im[k]=signal_received_block_im[((int) symb_combination[k])*symb_block_size];
        
        log_prob_symb_block[k]=log_prob_symb[(int) symb_combination[k]];
        for (jj=0;jj<symb_block_size-1;jj++)
        {
            
            sum_symb_combination=sum_symb_combination+symb_combination[k+jj*nb_combination];
            
            mu_symb_combination_re[k]=mu_symb_combination_re[k]+signal_received_block_re[jj+1+((int) symb_combination[k+(jj+1)*nb_combination] )*symb_block_size]*cos(2*pi*h*sum_symb_combination)+signal_received_block_im[jj+1+((int) symb_combination[k+(jj+1)*nb_combination] )*symb_block_size]*sin(2*pi*h*sum_symb_combination);
            
            mu_symb_combination_im[k]=mu_symb_combination_im[k]-signal_received_block_re[jj+1+((int) symb_combination[k+(jj+1)*nb_combination] )*symb_block_size]*sin(2*pi*h*sum_symb_combination)+signal_received_block_im[jj+1+((int) symb_combination[k+(jj+1)*nb_combination] )*symb_block_size]*cos(2*pi*h*sum_symb_combination);
            
            log_prob_symb_block[k]=log_prob_symb_block[k]+log_prob_symb[(int) symb_combination[k+(jj+1)*nb_combination]+(jj+1)*M];
        }
        sum_symb_combination=0;
    }
    
    if (channel_coherent)
    {
        for (k=0;k<nb_combination;k++)
        {
            prob_symb[k]=exp( (2*a*sqrt(Es)/N0)*(cos(phase_shift_begin_block)*mu_symb_combination_re[k]+sin(phase_shift_begin_block)*mu_symb_combination_im[k])+log_prob_symb_block[k] );
            sum_prob_symb=sum_prob_symb+prob_symb[k];
        }
        for (k=0;k<nb_combination;k++)
        {
            prob_symb[k]=prob_symb[k]/(double)sum_prob_symb;
            sum_prob_symb2=sum_prob_symb2+prob_symb[k];
        }
        
        *Z=log2(prob_symb[index]/(double)sum_prob_symb2);
    }
    else
    {
        for (k=0;k<nb_combination;k++)
        {
            prob_symb[k]=bessi0( (2*a*sqrt(Es)/N0)*sqrt(pow(mu_symb_combination_re[k],2)+pow(mu_symb_combination_im[k],2)))*exp(log_prob_symb_block[k]);
            sum_prob_symb=sum_prob_symb+prob_symb[k];
        }
        for (k=0;k<nb_combination;k++)
        {
            prob_symb[k]=prob_symb[k]/(double)sum_prob_symb;
            sum_prob_symb2=sum_prob_symb2+prob_symb[k];
        }
        *Z=log2(prob_symb[index]/(double)sum_prob_symb2);
        
    }
    
    free(mu_symb_combination_re);
    free(mu_symb_combination_im);
    free(log_prob_symb_block);
    
}
/* ===============================================================================================================*/
void Multisymb_decision_function(int M, int fin, int symb_block_size, double *binary_combination, double* prob_symb, double *prob_binary, int debut)
{
    int m=log2(M);
    int nb_combination=pow(M,symb_block_size);
    int column_N_D=((m*symb_block_size)/(double)2)*nb_combination;
    double* N = (double *) calloc ( fin*column_N_D , sizeof(double));
    double* D = (double *) calloc ( fin*column_N_D , sizeof(double));
    int j,k;
    int indexN=0;
    int indexD=0;
    
    for (j=0;j<fin;j++)
    {
        for (k=0;k<nb_combination;k++)
        {
            if (binary_combination[k+(j+debut)*nb_combination]==1)
            {
                N[j+indexN*m]=prob_symb[k];
                prob_binary[1+j*2]=prob_binary[1+j*2]+N[j+indexN*m];
                indexN=indexN+1;
            }
            else
            {
                D[j+indexD*m]=prob_symb[k];
                prob_binary[j*2]=prob_binary[j*2]+D[j+indexD*m];
                indexD=indexD+1;
                
            }
        }
        
    }
    free(N);
    free(D);
    
}
/* ===============================================================================================================*/
 /*############################################################################################################*/
 /*------------------------------------------------------- Starting ---------------------------------------------*/
 /*############################################################################################################*/

void Multisymb_detection_decision_c2_milieu_block( double *symb_frame,
                                                         double *symb_combination,
                                                         double *received_signal_matched_re,
                                                         double *received_signal_matched_im,
                                                         double h,
                                                         double *a,
                                                         double N0, 
                                                         double Es,
                                                         double *log_prob_symb,
                                                         bool channel_coherent,
                                                         int M,
                                                         int symb_block_size,
                                                         double *binary_combination,
                                                         int symb_frame_size, 
                                                         bool sliding,
														 double *APP_bit_frame,
														 double *Z_tab)


{
    
    
    // ========================================================================================

    int m;
    int binary_frame_size;
    int nb_combination;
    
    double *set_symb_block;
    double *signal_received_block_re;
    double *signal_received_block_im;
    double *log_prob_symb_windowed;
    double *prob_symb;
    
    double *Z;
    int index;
    /*int j,k,jj,fin, sliding_increment, b_size, q, sum_q;*/
    int j,k,jj,fin, sliding_increment, q, sum_q;
    double llr;
    double phase_shift_begin_block;
    int binary_block_size;
    int debut;
    
    double *b;
    double *prob_binary;
    
    
    m=log2((double)M);
    binary_frame_size=symb_frame_size*m;
    nb_combination=pow((double)M,(double)symb_block_size);

    // ========================================================================================
    
    
    m=log2((double)M);
    binary_frame_size=symb_frame_size*m;
    nb_combination=pow((double)M,(double)symb_block_size);
    
	/*Declaration outputs*/
    double *prob_binary_tab;

    /*Declaration locals*/
    set_symb_block = (double *) malloc ( symb_block_size * sizeof(double));
    signal_received_block_re=(double *) malloc ( symb_block_size*M * sizeof(double));
    signal_received_block_im=(double *) malloc ( symb_block_size*M * sizeof(double));
    log_prob_symb_windowed=(double *) malloc ( M*symb_block_size * sizeof(double));
    prob_symb=(double *) calloc ( nb_combination , sizeof(double));
    prob_binary_tab=(double *) malloc ( 2*binary_frame_size * sizeof(double));
   
   /* treatments */
   Z =(double *) calloc ( 1 , sizeof(double));
    index=0;
    phase_shift_begin_block=0;
    binary_block_size=symb_block_size*m;
    debut=0;
    
    if (sliding)
    {
        sliding_increment=1;
        /*b_size=m;*/
    }
    else
    {
        sliding_increment=symb_block_size;
        /*b_size=binary_block_size;*/
    }
    
    /*############################################################################################################*/
    /*-------------------------------------------------------Sorties---------------------------------------------*/
    /*############################################################################################################*/
    
    for(j=0;j<symb_frame_size-symb_block_size+1;j+=sliding_increment)
    {
        for (k=0;k<symb_block_size;k++)
        {
            set_symb_block[k]=symb_frame[j+k];
            for(jj=0;jj<M;jj++)
            {
                signal_received_block_re[k+symb_block_size*jj]=received_signal_matched_re[j+k+symb_frame_size*jj];
                signal_received_block_im[k+symb_block_size*jj]=received_signal_matched_im[j+k+symb_frame_size*jj];
                log_prob_symb_windowed[jj+k*M]=log_prob_symb[jj+(j+k)*M];
            }
        }
        
        //printf("OK-\n");
        Multisymb_detection_function(M, symb_block_size, set_symb_block, symb_combination, log_prob_symb_windowed, signal_received_block_re, signal_received_block_im, h, channel_coherent, prob_symb, Z, a[j], Es,N0, phase_shift_begin_block, nb_combination);
        //printf("OK+\n");
        Z_tab[j]=*Z;
        
        if (sliding)
        {
            if (j<symb_frame_size-symb_block_size && j>symb_block_size)
            {
                fin=m;
                debut=((int)(symb_block_size+1)/2)*m-m;
            }
            else
            {
                fin=binary_block_size;
                debut=0;
            }
        }
        else
        {
            fin=binary_block_size;
            if(j>=symb_frame_size-symb_block_size-2 && symb_frame_size % symb_block_size != 0)
            {
                if (j<symb_frame_size-symb_block_size)
                {
                    fin=m;
                }
                else
                {
                    fin=binary_block_size;
                }
            }
            
        }
        
        b= (double *) malloc ( fin * sizeof(double));
        
        prob_binary=(double *) calloc ( 2*fin , sizeof(double));
        
        Multisymb_decision_function(M, fin, symb_block_size, binary_combination, prob_symb, prob_binary, debut);        
        
        for (k=0;k<2;k++)
        {
            for(jj=0;jj<fin;jj++)
            {
                prob_binary_tab[k+(jj+debut+index)*2]=prob_binary[k+jj*2];
                llr=log(prob_binary[jj*2]/(double)prob_binary[1+jj*2]);
                if (j<symb_frame_size-symb_block_size)
                {
                    if (llr>0)
                    {
                        b[jj]=0;
                    }
                    else
                    {
                        b[jj]=1;
                    }
                }
            }
        }
        
        if (sliding)
        {
            index+=m;
        }
        else
        {
            if(j>=symb_frame_size-symb_block_size-2 && symb_frame_size % symb_block_size != 0)
            {
                index+=m;
            }
            else
            {
                index+=binary_block_size;
            }
        }
        free(prob_binary);
        sum_q=0;
        if (j<symb_frame_size-symb_block_size)
        {
            if (M==2)
            {
                for(k=0;k<fin;k+=m)
                {
                    sum_q+=b[k];
                }
            }
            
            if (M==4)
            {
                for(k=0;k<fin;k+=m)
                {
                    if(b[k]==0 && b[k+1]==0)
                    {
                        q=0;
                    }
                    if(b[k]==1 && b[k+1]==0)
                    {
                        q=1;
                    }
                    if(b[k]==0 && b[k+1]==1)
                    {
                        q=2;
                    }
                    if(b[k]==1 && b[k+1]==1)
                    {
                        q=3;
                    }
                    sum_q+=q;
                }
            }
            if (M==8)
            {
                for(k=0;k<fin;k+=m)
                {
                    if(b[k]==0 && b[k+1]==0 && b[k+2]==0)
                    {
                        q=0;
                    }
                    if(b[k]==1 && b[k+1]==0 && b[k+2]==0)
                    {
                        q=1;
                    }
                    if(b[k]==0 && b[k+1]==1 && b[k+2]==0)
                    {
                        q=2;
                    }
                    if(b[k]==1 && b[k+1]==1 && b[k+2]==0)
                    {
                        q=3;
                    }
                    if(b[k]==0 && b[k+1]==0 && b[k+2]==1)
                    {
                        q=4;
                    }
                    if(b[k]==1 && b[k+1]==0 && b[k+2]==1)
                    {
                        q=5;
                    }
                    if(b[k]==0 && b[k+1]==1 && b[k+2]==1)
                    {
                        q=6;
                    }
                    if(b[k]==1 && b[k+1]==1 && b[k+2]==1)
                    {
                        q=7;
                    }
                    sum_q+=q;
                }
            }
            phase_shift_begin_block=fmod(2*pi*sum_q*h+phase_shift_begin_block,2*pi);
        }

        free(b);
        if(j>=symb_frame_size-symb_block_size-2 && symb_frame_size % symb_block_size != 0)
        {
            if(!sliding)
            {
                sliding_increment=1;
            }
        }
    }
    
    // ==================================================================
    
    
    for(jj=0;jj<binary_frame_size;jj++)
        {
            for (k=0;k<2;k++)
            {
              llr=log(prob_binary_tab[jj*2])-log(prob_binary_tab[1+jj*2]);
              APP_bit_frame[jj] = llr;
            }
		}
    
    // ==================================================================
  
    free(set_symb_block); 
    free(signal_received_block_re); 
    free(signal_received_block_im); 
    free(log_prob_symb_windowed); 
    free(prob_symb); 
    free(prob_binary_tab); 
    free(Z); 

}											   
