
#include <math.h>
#include <string.h>


void lte_crc_cal(int *InputBits,
                  int nInputBits,
                  const char *crc_type,
                  int *crc_bits           
                  );


void lte_crc_cal(int *InputBits,
                  int nInputBits,
                  const char *crc_type,
                  int *crc_bits           
                  )
{
 
   if ((strcmp(crc_type,"8")== 0) || (strcmp(crc_type,"16")== 0) || (strcmp(crc_type,"24A")== 0) || (strcmp(crc_type,"24B")== 0))
   {    
     
        if (strcmp(crc_type,"8")== 0){

            int crc_len  = 8;            
            int *crc_poly= (int*) calloc(crc_len+1, sizeof(int));
            int *crc_rem  = (int*) calloc(crc_len+1, sizeof(int));
            
            crc_poly[0]=1;
            crc_poly[1]=1;
            crc_poly[2]=0;
            crc_poly[3]=0;
            crc_poly[4]=1;
            crc_poly[5]=1;
            crc_poly[6]=0;
            crc_poly[7]=1;
            crc_poly[8]=1;
            
            
            int m,n;
			int tmp_value;
			
			for (m=0;m<crc_len;m++){
				crc_bits[m]= 0;
			} 
			/* =======================================================*/
			
			for (n=0;n<nInputBits+crc_len;n++){ 
				 
				 if (n<nInputBits)  tmp_value=(int) InputBits[n];
				 else               tmp_value=0; 
				/* =================================================*/
				for (m=0;m<crc_len;m++){crc_rem[m]= crc_bits[m];}
				crc_rem[crc_len]=tmp_value;
			   /* =================================================*/
			   if (crc_rem[0] !=0){for (m=0;m<crc_len+1;m++){crc_rem[m]= (crc_rem[m]+crc_poly[m])%2;} } 
			   /* ================== Update CRC ===================*/
			   for (m=0;m<crc_len;m++){crc_bits[m]= crc_rem[m+1];} 
			   /* =================================================*/
			} 

			
			free(crc_poly);
			free(crc_rem);	
			
           } 
        else if (strcmp(crc_type,"16")== 0){

            int crc_len  = 16;
            int *crc_poly= (int*) calloc(crc_len+1, sizeof(int));
             int *crc_rem  = (int*) calloc(crc_len+1, sizeof(int));
            
            crc_poly[0]=1;
            crc_poly[1]=0;
            crc_poly[2]=0;
            crc_poly[3]=0;
            crc_poly[4]=1;
            crc_poly[5]=0;
            crc_poly[6]=0;
            crc_poly[7]=0;
            crc_poly[8]=0;
            crc_poly[9]=0;
            crc_poly[10]=0;
            crc_poly[11]=1;
            crc_poly[12]=0;
            crc_poly[13]=0;
            crc_poly[14]=0;
            crc_poly[16]=1;
            
            int m,n;
			int tmp_value;
			
			for (m=0;m<crc_len;m++){
				crc_bits[m]= 0;
			} 
			/* =======================================================*/
			
			for (n=0;n<nInputBits+crc_len;n++){ 
				 
				 if (n<nInputBits)  tmp_value=(int) InputBits[n];
				 else               tmp_value=0; 
				/* =================================================*/
				for (m=0;m<crc_len;m++){crc_rem[m]= crc_bits[m];}
				crc_rem[crc_len]=tmp_value;
			   /* =================================================*/
			   if (crc_rem[0] !=0){for (m=0;m<crc_len+1;m++){crc_rem[m]= (crc_rem[m]+crc_poly[m])%2;} } 
			   /* ================== Update CRC ===================*/
			   for (m=0;m<crc_len;m++){crc_bits[m]= crc_rem[m+1];} 
			   /* =================================================*/
			} 
			
			
			free(crc_poly);
			free(crc_rem);	
			
          }
        else if (strcmp(crc_type,"24A")== 0){

            int crc_len  = 24;
            int *crc_poly= (int*) calloc(crc_len+1, sizeof(int));
	    int *crc_rem  = (int*) calloc(crc_len+1, sizeof(int));
             
            crc_poly[0]=1;
            crc_poly[1]=1;
            crc_poly[2]=0;
            crc_poly[3]=0;
            crc_poly[4]=0;
            crc_poly[5]=0;
            crc_poly[6]=1;
            crc_poly[7]=1;
            crc_poly[8]=0;
            crc_poly[9]=0;
            crc_poly[10]=1;
            crc_poly[11]=0;
            crc_poly[12]=0;
            crc_poly[13]=1;
            crc_poly[14]=1;
            crc_poly[15]=0;
            crc_poly[16]=0;
            crc_poly[17]=1;
            crc_poly[18]=1;
            crc_poly[19]=1;
            crc_poly[20]=1;
            crc_poly[21]=1;
            crc_poly[22]=0;
            crc_poly[23]=1;
            crc_poly[24]=1;
            
            int m,n;
			int tmp_value;
			
			for (m=0;m<crc_len;m++){
				crc_bits[m]= 0;
			} 
			/* =======================================================*/
			
			for (n=0;n<nInputBits+crc_len;n++){ 
				 
				 if (n<nInputBits)  tmp_value=(int) InputBits[n];
				 else               tmp_value=0; 
				/* =================================================*/
				for (m=0;m<crc_len;m++){crc_rem[m]= crc_bits[m];}
				crc_rem[crc_len]=tmp_value;
			   /* =================================================*/
			   if (crc_rem[0] !=0){for (m=0;m<crc_len+1;m++){crc_rem[m]= (crc_rem[m]+crc_poly[m])%2;} } 
			   /* ================== Update CRC ===================*/
			   for (m=0;m<crc_len;m++){crc_bits[m]= crc_rem[m+1];} 
			   /* =================================================*/
			} 

			
			free(crc_poly);
			free(crc_rem);	
			
           }      
        else if (strcmp(crc_type,"24B")== 0){

            int crc_len  = 24;
            int *crc_poly= (int*) calloc(crc_len+1, sizeof(int));
            int *crc_rem  = (int*) calloc(crc_len+1, sizeof(int));
            
            crc_poly[0]=1;
            crc_poly[1]=1;
            crc_poly[2]=0;
            crc_poly[3]=0;
            crc_poly[4]=0;
            crc_poly[5]=0;
            crc_poly[6]=0;
            crc_poly[7]=0;
            crc_poly[8]=0;
            crc_poly[9]=0;
            crc_poly[10]=0;
            crc_poly[11]=0;
            crc_poly[12]=0;
            crc_poly[13]=0;
            crc_poly[14]=0;
            crc_poly[15]=0;
            crc_poly[16]=0;
            crc_poly[17]=0;
            crc_poly[18]=1;
            crc_poly[19]=1;
            crc_poly[20]=0;
            crc_poly[21]=0;
            crc_poly[22]=0;
            crc_poly[23]=1;
            crc_poly[24]=1;
            
            int m,n;
			int tmp_value;
			
			for (m=0;m<crc_len;m++){
				crc_bits[m]= 0;
			} 
			/* =======================================================*/
			
			for (n=0;n<nInputBits+crc_len;n++){ 
				 
				 if (n<nInputBits)  tmp_value=(int) InputBits[n];
				 else               tmp_value=0; 
				/* =================================================*/
				for (m=0;m<crc_len;m++){crc_rem[m]= crc_bits[m];}
				crc_rem[crc_len]=tmp_value;
			   /* =================================================*/
			   if (crc_rem[0] !=0){for (m=0;m<crc_len+1;m++){crc_rem[m]= (crc_rem[m]+crc_poly[m])%2;} } 
			   /* ================== Update CRC ===================*/
			   for (m=0;m<crc_len;m++){crc_bits[m]= crc_rem[m+1];} 
			   /* =================================================*/
			} 

			
			free(crc_poly);
			free(crc_rem);	
           }
        
         /* ================ Initialisation =======================*/
    }
    else
    {
        printf("CRC type is not matched");
        return;
    }    
 }
