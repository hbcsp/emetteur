#include "math.h"


void generate_HDR(int hdrLen, int user_number, int FrameCount, int FrameSize, int *HDR);

void Read_HDR(int *HDR, int *sortie_user_number, int *sortie_FrameCount, int *sortie_FrameSize);


void generate_HDR(int hdrLen, int user_number, int FrameCount, int FrameSize, int *HDR)
	{   
		int n,j;
	    int unsigned short valeur;
	    int nbBitPerdecim;
	    int decim;
	    int start_index;
	    
	    // HDR(1:2:end)=0;  HDR(2:2:end)=1;
		for(n=0;n<hdrLen;n++)
	    { 
		 if (n %2 == 0)  HDR[n] = 0;
		 else            HDR[n] = 1;	 
		}
		// HDR(1:8)   =  de2bi(user_number,8,'left-msb'); 
		decim = user_number;
		nbBitPerdecim = 8;
		start_index = 0;
		
		valeur = decim;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        HDR[start_index+j] = valeur % 2;
	        valeur = valeur/2;
		}
		// HDR(9:16)  =  [1 0 1 0 0 0 1 0];
		HDR[8] = 1;  HDR[9] = 0;  HDR[10] = 1;   HDR[11] = 0;  HDR[12] = 0;  HDR[13] = 0;  HDR[14] = 1;  HDR[15] = 0;
		
		// HDR(17:24)  =  de2bi(mod(FrameCount,256),8,'left-msb');
		decim = FrameCount % 256;
		nbBitPerdecim = 8;
		start_index = 16;
		
		valeur = decim;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        HDR[start_index+j] = valeur % 2;
	        valeur = valeur/2;
		}
		
		// HDR(89:104) = de2bi(FrameSize,16,'left-msb');
		decim = FrameSize;
		nbBitPerdecim = 16;
		start_index = 88;
		
		valeur = decim;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        HDR[start_index+j] = valeur % 2;
	        valeur = valeur/2;
		}
	}  


 void Read_HDR(int *HDR, int *sortie_user_number, int *sortie_FrameCount, int *sortie_FrameSize)
	{   
        int j;	    
	    
	    int start_index;
	    int nbBitPerdecim;
	    int sum_value;
	    
       // HDR(1:8)   =  de2bi(user_number,8,'left-msb'); 	
		start_index = 0;
		nbBitPerdecim = 8;
		
		sum_value = 0;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        sum_value = sum_value + (HDR[start_index+j]* (1 << j));
		}
		*sortie_user_number =  sum_value;
		
		// HDR(17:24)  =  de2bi(mod(FrameCount,256),8,'left-msb');
		start_index = 16;
		nbBitPerdecim = 8;
		
		sum_value = 0;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        sum_value = sum_value + (HDR[start_index+j]* (1 << j));
		}
		*sortie_FrameCount =  sum_value;
		
		// HDR(89:104) = de2bi(FrameSize,16,'left-msb');
		start_index = 88;
		nbBitPerdecim = 16;
		
		sum_value = 0;
	    for (j=0;j<nbBitPerdecim;j++)
	    {
	        sum_value = sum_value + (HDR[start_index+j]* (1 << j));
		}
		*sortie_FrameSize =  sum_value;
    }
