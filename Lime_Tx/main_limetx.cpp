
#include <iostream>
//#include <chrono>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include <termios.h>
#include <ctype.h>
#include <unistd.h> // usleep
#include <signal.h>
#include "LimeSuite.h"

#define PROGRAM_VERSION "0.0.1"

using namespace std;

FILE *input_fid;

//Device structure, should be initialize to NULL
static lms_device_t* m_dev = NULL;
double m_Bandwidth = 5000000;

#define BUFFER_SIZE 1000
#define FIFO_SIZE (1024*128) // BUFFER_SIZE*20

static bool keep_running=false;


typedef struct {
		int16_t re;
		int16_t im;
	    }i16cmplx;	    
	    

int finish()
{
    if (m_dev != NULL)
    {
	int status = LMS_Close(m_dev);
        if (status == 0)  fprintf(stderr,"[SDR RX] LimeSDR is Closed with failure.\n");
	m_dev = NULL;
    }
    exit(-1);
}

static void signal_handler(int signo)
{
    if (signo == SIGINT)
        fputs("\n[SDR TX] Caught SIGINT\n", stderr);
    else if (signo == SIGTERM)
        fputs("\n[SDR TX] Caught SIGTERM\n", stderr);
    else if (signo == SIGHUP)
        fputs("\n[SDR TX] Caught SIGHUP\n", stderr);	
    else if (signo == SIGQUIT)
        fputs("\n[SDR TX] Caught SIGQUIT\n", stderr);
    else if (signo == SIGKILL)
        fputs("\n[SDR TX] Caught SIGKILL\n", stderr);	
    else if (signo == SIGPIPE)
        fputs("\n[SDR TX] Received SIGPIPE.\n", stderr);
    else
        fprintf(stderr, "\nCaught signal: %d\n", signo);

    keep_running = false;
}



void print_usage()
{

	fprintf(stderr, \
		"limetx -%s\n\
Usage:\nlimetx -s SymbolRate [-i File Input] [-o File Output] [-f Frequency in Khz]  [-g Gain] [-t SampleType] [-h] \n\
-i            Input IQ File I16 format (default stdin) \n\
-s            SampleRate in KS \n\
-f            Frequency in Mhz\n\
-g            Gain 0 to 100\n\
-h            help (print this help).\n\
Example : ./limetx -sr 1000 -f 1242000 -g 80\n\
\n", \
PROGRAM_VERSION);

} /* end function print_usage */

int main(int argc, char** argv)
{
    
    const char *SerialNumber = "1D4C3A8E0456D0";	
    input_fid = stdin;
    int upsFactor=1;
    
    double SampleRate = 0.0;
    double CenterFrequency = 0.0;
    double Gain = -1.0;
    
    
    int a;
    int anyargs = 0;
     
    
    while (1)
    {
	a = getopt(argc, argv, "i:s:f:g:h:");

	if (a == -1)
	{
	    if (anyargs) break;
	    else a = 'h'; //print usage and exit
	}
	anyargs = 1;

	switch (a)
	{
	case 'i': // InputFile
		if ( optarg != NULL ) 
		{
		    input_fid = fopen(optarg, "rb");
		    if ( input_fid == NULL )
		    {
			    fprintf(stderr, "[SDR TX] Unable to open '%s': %s\n",optarg, strerror(errno));
			    exit(EXIT_FAILURE);
		    }
		}
		break;
	case 's': // SymbolRate in KS
		SampleRate = atol(optarg)*1000;
		break;
	case 'f': // TxFrequency in Mhz
		CenterFrequency = atol(optarg)*1000000;
		break;
	case 'g': // Gain 0..100
		Gain = atoi(optarg)/100.0;
		break;
	case 'h': // help
		print_usage();
		exit(0);
		break;
	case -1:
		break;
	case '?':
		if (isprint(optopt))
		{
		   fprintf(stderr, "[SDR TX] limetx `-%c'.\n", optopt);
		}
		else
		{
		   fprintf(stderr, "[SDR TX] limetx: unknown option character `\\x%x'.\n", optopt);
		}
		print_usage();

		exit(1);
		break;
	default:
		print_usage();
		exit(1);
		break;
	}/* end switch a */
    }/* end while getopt() */
    
    bool isapipe = (fseek(input_fid, 0, SEEK_CUR) < 0); //Dirty trick to see if it is a pipe or not
    if (isapipe)
	fprintf(stderr, "[SDR TX] Using IQ live mode\n");
    else
	fprintf(stderr, "[SDR TX] Read from data File\n");	
    
    if (CenterFrequency == 0) {fprintf(stderr, "[SDR TX] Need set a frequency to tx\n"); exit(0);}
    if (SampleRate == 0) {fprintf(stderr, "[SDR TX] Need set a SampleRate \n"); exit(0);}
    if (Gain == -1.0) {fprintf(stderr, "[SDR TX] Need set a Gain \n"); exit(0);}
    
    m_Bandwidth = (SampleRate *1.4 < 5000000) ?  5000000:SampleRate*1.4;
    
    // register signal handlers
    if (signal(SIGINT, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGINT\n", stderr);
    if (signal(SIGTERM, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGTERM\n", stderr);
    if (signal(SIGHUP, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGHUP\n", stderr);
    if (signal(SIGQUIT, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGQUIT\n", stderr);
    if (signal(SIGKILL, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGKILL\n", stderr);
    if (signal(SIGPIPE, signal_handler) == SIG_ERR)
        fputs("[SDR TX] Warning: Can not install signal handler for SIGPIPE\n", stderr);

    // =======================================================================================================================================================================================================
    // ########################################################################################################################################################################################################
    // =======================================================================================================================================================================================================
    int status;
    
    //Find devices
    int n;
    lms_info_str_t list[8];
    if ((n = LMS_GetDeviceList(list)) < 1)
    {
	fprintf(stderr,"\n[SDR TX] No Lime found\n");
	finish();
	return -1;
    }    
    
    //open the limeSDR device with designed Serial Number
    if (m_dev == NULL) {
	int i ;
	fprintf(stderr,"\n [SDR TX] Available device list is: \n");
	for (i = 0; i < n; i++) fprintf(stderr,"[SDR TX] %s \n",list[i]);
	for (i = 0; i < n; i++) if ((strstr(list[i], "LimeSDR") != NULL) && (strstr(list[i], SerialNumber) != NULL)) break;
	if (i == n) 
	{ 
	    fprintf(stderr,"[SDR TX] LimeSDR device with serial number [%s] is not found \n",SerialNumber);
	    finish();
	    return -1;
	}
	status = LMS_Open(&m_dev, list[i], NULL);
	if(status !=0) 
	{
	    fprintf(stderr,"[SDR TX] LMS_Open() : %s \n",LMS_GetLastErrorMessage());
	    finish();
	    return -1;
	}
    }

    // Reset device
    status = LMS_Reset(m_dev) ;
    if (status < 0) {		
	fprintf(stderr,"[SDR TX] LMS_Reset() : %s \n",LMS_GetLastErrorMessage());
	finish();
	return -1;
    }
    
    //Initialize device with default configuration
    status = LMS_Init(m_dev);
    if (status!=0){
       fprintf(stderr,"[SDR TX] LMS_Init() : %s \n",LMS_GetLastErrorMessage());
       finish();
    }
      
    // ==========================================================================================
    
    //Disable RX channel,Channels are numbered starting at 0	
    status = LMS_EnableChannel(m_dev, LMS_CH_TX, 0, false);
    if (status != 0){
       fprintf(stderr,"[SDR TX] LMS_EnableChannel() : %s \n",LMS_GetLastErrorMessage());
       finish();
    }

    //Enable RX channel,Channels are numbered starting at 0
    status = LMS_EnableChannel(m_dev, LMS_CH_TX, 0, true);
    if (status != 0){
       fprintf(stderr,"[SDR TX] LMS_EnableChannel() : %s \n",LMS_GetLastErrorMessage()); 
       finish();
    }
    //Set sample rate
    status = LMS_SetSampleRate(m_dev, SampleRate, 0);
    if (status!=0){
	fprintf(stderr,"[SDR TX] LMS_SetSampleRate() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    
    // Set alalog LPF filter
    status = LMS_SetLPFBW(m_dev,LMS_CH_TX,0,m_Bandwidth);
    if (status!=0){
	fprintf(stderr,"[SDR TX] LMS_SetLPFBW() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    
    //select TX antenna path
    status = LMS_SetAntenna(m_dev, LMS_CH_TX, 0, LMS_PATH_TX2) ; // {LMS_CH_TX :  LMS_PATH_NONE = 0 ,LMS_PATH_TX1 = 1 , LMS_PATH_TX2 = 2 , LMS_PATH_AUTO = 255}
    if (status != 0){ 
	fprintf(stderr,"[SDR TX] LMS_SetAntenna() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    
    //Set center frequency 
    status = LMS_SetLOFrequency(m_dev,LMS_CH_TX, 0, CenterFrequency);
    if (status != 0) {
	fprintf(stderr,"[SDR TX] LMS_SetLOFrequency() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }

    //set TX gain
    status = LMS_SetNormalizedGain(m_dev, LMS_CH_TX, 0, Gain);
    if (status != 0){
	fprintf(stderr,"[SDR TX] LMS_SetNormalizedGain() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    
    
    
    //calibrate Tx, continue on failure
    status = LMS_Calibrate(m_dev, LMS_CH_TX, 0,m_Bandwidth, 0);
    if (status != 0){
	fprintf(stderr,"[SDR TX] LMS_Calibrate() : %s \n",LMS_GetLastErrorMessage());
        finish();
    }
    // save Calibration
    status = LMS_SaveConfig(m_dev, "limemini.cal");
    if (status < 0){
	fprintf(stderr,"[SDR TX] LMS_SaveConfig() : %s \n",LMS_GetLastErrorMessage());
	return -1;
    }
    
 
    // =======================================================================================================================================================================================================
    // ########################################################################################################################################################################################################
    // =======================================================================================================================================================================================================  
      keep_running=true;
      
      float_type ggain,Fc,HostSR, DacSR,bw;
      LMS_GetNormalizedGain(m_dev, LMS_CH_TX, 0, &ggain);
      LMS_GetLOFrequency(m_dev, LMS_CH_TX, 0, &Fc);
      LMS_GetSampleRate(m_dev, LMS_CH_TX, 0, &HostSR, &DacSR);
      LMS_GetLPFBW(m_dev, LMS_CH_TX, 0, &bw);
      
      lms_range_t LOF_Range,SR_Range,AntBW_Range,LPFBW_Range;
      LMS_GetLOFrequencyRange(m_dev, LMS_CH_TX, &LOF_Range);   // LOF_Range.min   LOF_Range.max   LOF_Range.step
      LMS_GetSampleRateRange(m_dev, LMS_CH_TX, &SR_Range);   // SR_Range.min   SR_Range.max   SR_Range.step
      LMS_GetLPFBWRange(m_dev, LMS_CH_TX, &LPFBW_Range);   // LPFBW_Range.min   LPFBW_Range.max   LPFBW_Range.step
      
      
     
      int ant_index_val=LMS_GetAntenna(m_dev, LMS_CH_TX, 0);  
      if (ant_index_val==1)
      {
           LMS_GetAntennaBW(m_dev, LMS_CH_TX, 0, LMS_PATH_TX1, &AntBW_Range);   // AntBW_Range.min   AntBW_Range.max   AntBW_Range.step
	   fprintf(stderr,"[SDR TX] Selected RX path: BAND1. Reserved Antenna BW = %2.2f-%2.2f [MHz] by %2.2f [KHz] step \n", AntBW_Range.min/1e6, AntBW_Range.max/1e6, AntBW_Range.step/1e3);
       }
      else if (ant_index_val==2)
      {
           LMS_GetAntennaBW(m_dev, LMS_CH_TX, 0, LMS_PATH_TX2, &AntBW_Range);   // AntBW_Range.min   AntBW_Range.max   AntBW_Range.step
	   fprintf(stderr,"[SDR TX] Selected RX path: BAND2. Reserved Antenna BW = %2.2f-%2.2f [MHz] by %2.2f [KHz] step \n", AntBW_Range.min/1e6, AntBW_Range.max/1e6, AntBW_Range.step/1e3);
       }
      else 
	   fprintf(stderr,"[SDR TX] Error antenna index configuration.\n");
	   

      fprintf(stderr,"[SDR TX] Normalized Gain in = %2.2f  out = %2.2f \n", Gain,ggain);
      fprintf(stderr,"[SDR TX] Valid LO Frequency=%2.2f-%2.2f [MHz] by %2.2f [KHz] step : CenterFrequency in = %2.2f [MHz] out = %2.2f [MHz]\n", LOF_Range.min/1e6, LOF_Range.max/1e6, LOF_Range.step/1e3,CenterFrequency/1e6,Fc/1e6);
      fprintf(stderr,"[SDR TX] Valid SampleRate=%2.2f-%2.2f [MHz] by %2.2f [KHz] step : SampleRate in = %2.2f [MHz] out = %2.2f [MHz]\n", SR_Range.min/1e6, SR_Range.max/1e6, SR_Range.step/1e3,SampleRate/1e6,HostSR/1e6); 
      fprintf(stderr,"[SDR TX] Valid LPFBW=%2.2f-%2.2f [MHz] by %2.2f [KHz] step : LPFBW in = %2.2f [MHz] out = %2.2f [MHz]\n", LPFBW_Range.min/1e6, LPFBW_Range.max/1e6, LPFBW_Range.step/1e3,m_Bandwidth/1e6,bw/1e6);
      fprintf(stderr,"[SDR TX] DAC SampleRate=%2.2f [MHz]\n", DacSR/1e6);
      
    // =======================================================================================================================================================================================================
    // ########################################################################################################################################################################################################
    // =======================================================================================================================================================================================================   

    //Streaming Setup
    static lms_stream_t tx_stream;                 //stream structure

    tx_stream.channel = 0;            //channel number
    tx_stream.fifoSize = FIFO_SIZE;          //fifo size in samples
    tx_stream.throughputVsLatency = 0.2;    //0 min latency, 1 max throughput
    tx_stream.isTx = LMS_CH_TX;                  //TX channel (LMS_CH_TX = true; LMS_CH_TX = false)
    tx_stream.dataFmt = lms_stream_t::LMS_FMT_I16; //floating point sampl
    
    status = LMS_SetupStream(m_dev, &tx_stream);   
    if (status < 0 ) {
       fprintf(stderr,"[SDR TX] LMS_SetupStream() : %s \n",LMS_GetLastErrorMessage());
       return -1;
    }  
    
    //Set GFIR 

    if(upsFactor>1)
	LMS_SetGFIR(m_dev, LMS_CH_TX, 0, LMS_GFIR3, true);
    else
	LMS_SetGFIR(m_dev, LMS_CH_TX, 0, LMS_GFIR3, false);
    
    // setup meta data
    lms_stream_meta_t tx_metadata; //Use metadata for additional control over sample send function behavior
    tx_metadata.flushPartialPacket = false; //do not force sending of incomplete packet
    tx_metadata.waitForTimestamp = true; //Enable synchronization to HW timestamp
    int postpone_emitting_sec = 3;
    tx_metadata.timestamp = postpone_emitting_sec * SampleRate;

    // --------------------------------------------------------------------------------------------------------
    // ########################################################################################################
    // --------------------------------------------------------------------------------------------------------
    // active [bool] : indicates whether the stream is currently active
    // fifoFilledCount [uint32_t] : Number of samples in FIFO buffer
    // fifoSize [uint32_t] : Sise of FIFO buffer
    // underrun [uint_32] : FIFO underrun count
    // overrun [uint32_t] : FIFO overrun count
    // droppedPackets [uint32_t] : number of dropped packets by HW
    // sampleRate [float_type] : currently not used
    // linkRate [float_type] : combined data rate of all the same direction (Tx or Rx)
    // timestamp [uint64_t] : Current HW timestamp
    
    // ---------------------------------------------
    i16cmplx tx_buffer[BUFFER_SIZE];    //buffer to hold complex values 
    int nRead,nb_samples;
    bool repeat_sending = true;
    lms_stream_status_t TxStatus;
    // ---------------------------------------------
    // Set normalized gain to zero
    status = LMS_SetNormalizedGain(m_dev, LMS_CH_TX, 0, 0);
    if (status != 0){
	fprintf(stderr,"[SDR TX] LMS_SetNormalizedGain() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    
    bool FirstTx=true;
    bool Transition=true;
    int TotalSampleSent=0;
    static int debugCnt = 0;
    
    if (keep_running)
     fprintf(stderr,">>>> [SDR RX] starting data sent ......\n");
    
    while( keep_running )
    { 
	LMS_GetStreamStatus(&tx_stream, &TxStatus);
	
	
	
	if(FirstTx&&(TxStatus.fifoFilledCount==TxStatus.fifoSize))
	{
	    fprintf(stderr,"[SDR TX] Restart stream %d \n",TxStatus.fifoFilledCount);
	    LMS_StartStream(&tx_stream);
	    FirstTx=false;
	}
	
	if((!FirstTx)&&(TxStatus.fifoFilledCount<TxStatus.fifoSize*0.25))
	{
	    memset(tx_buffer,0,BUFFER_SIZE*sizeof(i16cmplx));
	    for(int i=0;i<8;i++)
	    {
		LMS_SendStream( &tx_stream,tx_buffer,BUFFER_SIZE,nullptr,1000);
	    }
	    fprintf(stderr,"[SDR TX] Underflow ! %d\n",TxStatus.fifoFilledCount);
	    
	}
	
	
	
	// Fill Tx Buffer
	nRead = fread(tx_buffer,sizeof(i16cmplx),BUFFER_SIZE,input_fid);
	
	if (nRead < BUFFER_SIZE) 
	{ 
	   if (nRead > 0) fprintf(stderr,"[SDR TX] Incomplete buffer %d/%d\n", nRead, BUFFER_SIZE);
	   else	
	   {	 
	       if (isapipe) break;
	       else
	       {
		 if (repeat_sending)  
		 {
		     fseek(input_fid,0, SEEK_SET);
		     continue;
		 }
	         else 
		     break;
	       }
	    }
	}
	
	
	// Send samples
	if(!FirstTx)
	   nb_samples = LMS_SendStream(&tx_stream,tx_buffer,nRead, nullptr, 1000 );
	else
	   nb_samples = LMS_SendStream(&tx_stream,tx_buffer,nRead, nullptr, 000 );
	
	if ((nb_samples < 0) || (nb_samples != nRead))
	{
	   if (nb_samples != nRead) fprintf(stderr,"[SDR TX] error: samples sent: %d/%d \n",nb_samples,nRead); 
	   if (nb_samples < 0)  
	   {
	       fprintf(stderr,"[SDR TX] LMS_SendStream() : %s \n",LMS_GetLastErrorMessage());
	       break;
	   }
	}
	TotalSampleSent+=nb_samples;
	tx_metadata.timestamp += nb_samples;
	
	if(Transition)
	{
	    if(TotalSampleSent>SampleRate) // 1 second
	    {
		//set TX gain
		status = LMS_SetNormalizedGain(m_dev, LMS_CH_TX, 0, Gain);
		if (status != 0){
		    fprintf(stderr,"[SDR TX] LMS_SetNormalizedGain() : %s \n",LMS_GetLastErrorMessage());
		    finish();
		}
		
		Transition=false;		
	    }
	}

	
	
	
       if ((debugCnt % 100) == 0)
       {
	  fprintf(stderr,"[SDR TX] FIFO =%d/%d\n",TxStatus.fifoFilledCount,TxStatus.fifoSize);
       }    
        
        debugCnt++ ;
    }
    
    // close binary file
     fclose(input_fid);  
     fprintf(stderr,"[SDR TX] Input File is Closed with success.\n");
     
    // Set normalized gain to zero  
    status = LMS_SetNormalizedGain(m_dev, LMS_CH_TX, 0, 0);
    if (status != 0){
	fprintf(stderr,"[SDR TX] LMS_SetNormalizedGain() : %s \n",LMS_GetLastErrorMessage());
    	finish();
    }
    //Stop streaming
    LMS_StopStream(&tx_stream);
    //Disable RX channel
    status = LMS_EnableChannel(m_dev, LMS_CH_TX, 0, false);
    if (status != 0)  finish();
    // Destroy Rx streaming
    LMS_DestroyStream(m_dev, &tx_stream);
    //Close device
    status = LMS_Close(m_dev);
    if (status == 0)  fprintf(stderr,"[SDR TX] LimeSDR is closed \n");
    m_dev = NULL;
    //free(rx_buffer);
    return 0;
}
