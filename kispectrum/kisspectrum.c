#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h>
#include <liquid/liquid.h>
#include "VG/openvg.h"
#include "VG/vgu.h"
#include "fontinfo.h"
#include "shapes.h"
#include "colormaps.py"

#define pi 3.141592653589793


#define PROGRAM_VERSION "0.0.1"
#define WALPHA(x)  (x << 24)
#define WBLUE(x)   (x << 16)
#define WGREEN(x)  (x << 8)
#define WRED(x)    (x)
#define FFT_SIZE (1024)




FILE *input_fid;

typedef struct {
                int16_t re;
                int16_t im;
			}i16cmplx;
			
    
static unsigned int m_ColorTbl[256];

unsigned int setRgb(int r,int g,int b)
{
    return r|(g<<8)|(b<<16)|(0xFF<<24);
}

static void InitColorWaterFall()
{
  // default waterfall color scheme
  for (int i = 0; i < 256; i++){m_ColorTbl[i]=setRgb(_viridis_data[i][0]*255,_viridis_data[i][1]*255,_viridis_data[i][2]*255);} 
}

unsigned int SetColorFromFloat(float value)
{
    if(value>1.0) value=1.0;
    if(value<0.0) value=0.0;
    int Index=(value*255.0);   
    
    return m_ColorTbl[Index];
}

unsigned int SetColorFromInt(int value)
{
    if(value<0) value=0;
    if(value>255) value=255;
    int Index=(value);   
    
    return m_ColorTbl[Index];
}



#ifdef __ARM_NEON__
/*
void u8tofloat(
    size_t nsamples,
    unsigned char* buf,
    std::complex<float>* fo)
 {
  // Number to subtract from samples for normalization
  // See http://cgit.osmocom.org/gr-osmosdr/tree/lib/rtl/rtl_source_c.cc#n176
  const float32_t norm_ = 127.4f / 128.0f;
  float32x4_t norm = vld1q_dup_f32(&norm_);
  // Iterate over samples in blocks of 4 (each sample has I and Q)
  for (uint32_t i = 0; i < (nsamples / 4); i++) {
    uint32x4_t ui;
    ui[0] = buf[i * 8 + 0];
    ui[1] = buf[i * 8 + 2];
    ui[2] = buf[i * 8 + 4];
    ui[3] = buf[i * 8 + 6];
    uint32x4_t uq;
    uq[0] = buf[i * 8 + 1];
    uq[1] = buf[i * 8 + 3];
    uq[2] = buf[i * 8 + 5];
    uq[3] = buf[i * 8 + 7];
    // Convert to float32x4 and divide by 2^7 (128)
    float32x4_t fi = vcvtq_n_f32_u32(ui, 7);
    float32x4_t fq = vcvtq_n_f32_u32(uq, 7);
    // Subtract to normalize to [-1.0, 1.0]
    float32x4x2_t fiq;
    fiq.val[0] = vsubq_f32(fi, norm);
    fiq.val[1] = vsubq_f32(fq, norm);
    // Store in output
    vst2q_f32((float*) (&fo[i * 4]), fiq);
  }
}
*/
#endif 

static void signal_handler(int signo)
{
    if (signo == SIGINT)
        fputs("\nCaught SIGINT\n", stderr);
    else if (signo == SIGTERM)
        fputs("\nCaught SIGTERM\n", stderr);
    else if (signo == SIGHUP)
        fputs("\nCaught SIGHUP\n", stderr);
    else if (signo == SIGPIPE)
        fputs("\nReceived SIGPIPE.\n", stderr);
    else
        fprintf(stderr, "[KISSPEC] \nCaught signal: %d\n", signo);
}




void print_usage()
{

	fprintf(stderr,\
"kisspectrum -%s\n\
Usage:kisspectrum -i File [-s SampleRate] [-r FrameRate][-h] \n\
-i            Input IQ File I16 format (default stdin) \n\
-s            SampleRate (default 1024K) \n\
-r            Display Framertae (default 25) \n\
-h            help (print this help).\n\
Example : .\\kisspectrum -i - \n\
\n", \
PROGRAM_VERSION);

} /* end function print_usage */


// coordpoint marks a coordinate, preserving a previous color
void coordpoint(VGfloat x, VGfloat y, VGfloat size, VGfloat pcolor[4]) {
	Fill(128, 0, 0, 0.3);
	Circle(x, y, size);
	setfill(pcolor);
}

    
int main(int argc, char *argv[]) {


    input_fid = stdin;
    unsigned int SampleRate=1024000;
    int fps=25;
    
    int a;
	int anyargs = 0;
    while (1)
	{
		a = getopt(argc, argv, "i:s:r:h");

		if (a == -1)
		{
			if (anyargs) break;
			else a = 'h'; //print usage and exit
		}
		anyargs = 1;
        switch (a)
		{
		 case 'i': // InputFile
            if (optarg != NULL)
            {
                input_fid = fopen(optarg, "rb");
                if (input_fid == NULL)
                {
                    fprintf(stderr, "[KISSPEC] Unable to open '%s': %s\n",optarg, strerror(errno));
                    exit(EXIT_FAILURE);
                }
            }
			break;
         case 's': // SampleRate in Symbol/s [Kbaus/s]
			SampleRate = atoi(optarg)*1000;  
			break;
         case 'r': // Frame/s 
			fps = atoi(optarg);
            if(fps>25) fps=25; //Fixme should be teh framerate of screen mode  
			break;
		 case 'h': // help
			print_usage();
			exit(0);
			break;
		 case -1:
			break;
		 case '?':
			if (isprint(optopt))
			{
				fprintf(stderr, "[KISSPEC] kisspectrum `-%c'.\n", optopt);
			}
			else
			{
				fprintf(stderr, "[KISSPEC] kisspectrum: unknown option character `\\x%x'.\n", optopt);
			}
			print_usage();

			exit(1);
			break;
		 default:
            print_usage();
			exit(1);
			break;
		}/* end switch a */
    }/* end while getopt() */
    
   
   
   
    int nFFT = FFT_SIZE;

    i16cmplx *iqin_i16 =(i16cmplx *) malloc(nFFT*sizeof(i16cmplx));
    float complex * iqin     = (float complex*) malloc(nFFT * sizeof(float complex));  
    float complex * fftout   = (float complex*) malloc(nFFT * sizeof(float complex));
    float complex *derot_out = (float complex*) malloc(nFFT * sizeof(float complex)); 

     // ============================================================================================
    if (signal(SIGINT, signal_handler) == SIG_ERR)
        fputs("Warning: Can not install signal handler for SIGINT\n", stderr);
    if (signal(SIGTERM, signal_handler) == SIG_ERR)
        fputs("Warning: Can not install signal handler for SIGTERM\n", stderr);
    if (signal(SIGHUP, signal_handler) == SIG_ERR)
        fputs("Warning: Can not install signal handler for SIGHUP\n", stderr);
    if (signal(SIGPIPE, signal_handler) == SIG_ERR)
        fputs("Warning: Can not install signal handler for SIGPIPE\n", stderr); 
    // ==============================================================================================
    InitColorWaterFall();
    
    fftplan fft_plan = fft_create_plan(nFFT, iqin, fftout, LIQUID_FFT_FORWARD, 0);

    // ==================================================================================
    //functions: init() & start()
    int width, height;
    init(&width, &height);				   // Graphics initialization
    
    //width = 720;
    //height = 480;
    
    fprintf(stderr,"[KISSPEC] Display resolution = %dx%d\n",width,height);
    Start(width, height);				   // Start the picture
    
    // resolution ecran:
    //480p:      width = 720,  height = 480
    //720p:      width = 1280, height = 720
    //1080p:     width = 1920, height = 1080
    //4K:        width = 3840, height = 2160
    //8K:        width = 7680, height = 4320

    
    // ==================================================================================
    VGfloat *PowerFFT_x   = (VGfloat*)malloc(sizeof(VGfloat)*nFFT);
    VGfloat *PowerFFT_y   = (VGfloat*)malloc(sizeof(VGfloat)*nFFT);
    VGfloat *Oscillo_x    = (VGfloat*)malloc(sizeof(VGfloat)*nFFT);
    VGfloat *Oscillo_y_re = (VGfloat*)malloc(sizeof(VGfloat)*nFFT);
    VGfloat *Oscillo_y_im = (VGfloat*)malloc(sizeof(VGfloat)*nFFT);

    int OscilloWidth=(width-nFFT);
    int ConstellationWidth=(width-nFFT);
    int WaterfallHeight=height/2;
    int WaterfallWidth=nFFT;

    // 32 bit RGBA FFT
    unsigned int *fftImage=malloc(WaterfallWidth*(WaterfallHeight)*sizeof(unsigned int));
    unsigned int *fftLine=malloc(WaterfallWidth*sizeof(unsigned int));
    // 32 bit RGBA Constellation
    unsigned int *fftImageConstellation=malloc(ConstellationWidth*(height/2)*sizeof(unsigned int));

     // *************************************************************************************************************

   
    
    int          ftype = LIQUID_FIRFILT_RRC; // filter type
    unsigned int k     = 8;                  // samples/symbol
    unsigned int m     = 3;                  // filter delay (symbols)
    float        beta  = 0.35f;               // filter excess bandwidth factor
    int          ms    = LIQUID_MODEM_BPSK;  // modulation scheme (can be unknown)

    symtrack_cccf symtrack = symtrack_cccf_create(ftype,k,m,beta,ms);
    iirfilt_crcf dc_blocker = iirfilt_crcf_create_dc_blocker(0.001);

    Background(0, 0, 0);				   // Black background
    Fill(255, 255, 255, 1);
    BackgroundRGB(0,0,0,1);
    End();


    Line(nFFT/2,0,nFFT/2,10);
    Stroke(0, 0, 255, 1);
    Line(0,height-300,256,height-300);
    StrokeWidth(2);  
    StrokeWidth(1);
    Stroke(150, 150, 200, 1);

    
    //int WaterfallY=0;

    //VGfloat stops[] = {
        //0.0, 1.0, 1.0, 1.0, 1.0,
        //0.5, 0.5, 0.5, 0.5, 1.0,
        //1.0, 0.0, 0.0, 0.0, 1.0
    //};

    int SkipSamples=(SampleRate-(nFFT*fps))/fps;
    SkipSamples=(SkipSamples/nFFT)*nFFT;
    int TheoricFrameRate=SampleRate/nFFT;
    int ShowFrame=TheoricFrameRate/fps;
    
    fprintf(stderr,"[KISSPEC] TheoricFrameRate %d=%d/%d Showframe =%d\n",TheoricFrameRate,SampleRate,nFFT,ShowFrame);
    if(SkipSamples<0) SkipSamples=0;
    fprintf(stderr,"[KISSPEC] Skip Samples = %d\n",SkipSamples);


    int SpectrumPosX=0;//(width-nFFT)/2;
    int SpectrumPosY=height/2;
    int WaterfallPosX=0;//(width-WaterfallWidth)/2;
    //int WaterfallPosY=0;
    int OscilloPosX=nFFT;
    int OscilloPosY=height/2;
    int ConstellationPosX=nFFT;
    int ConstellationPosY=0;

    float FloorDb=220.0;
    float ScaleFFT=1.0;    
    
    fprintf(stderr,"[KISSPEC] Warning displayed only 1 IQ buffer every %d\n",ShowFrame);
    float RemanenceRate=8;
    float dbpeak=-200;
    float dbAmp;
    unsigned int idbAmp,idbAmpColor;
    char sTitle[100];
    sprintf(sTitle,"HERMOD OSCILLO");
    
    int PointX,PointY,Point;
    
    float K = 1.38064852*1e-23; // constante de Boltzman
    float T = 50; // temperature en Celsuis
    float N0_Hz = K*(T+273.15); // Densité spectrale de puissance de bruit thermique
    float N0_Hz_dBm = 10.0*(log(N0_Hz)/log(10.0))+30.0; // equal to -174 dBm/Hz for ambiant temperature

    float ENR = 82.0; // ENR: excess noise ratio [dB]
    float sigma2_n_dBm = (N0_Hz_dBm + 10.0*(log(SampleRate)/log(10.0)))+ENR; // puissance de bruit thermique ajouté [dBm]
    float sigma2_n = pow(10.0,(sigma2_n_dBm -30)/10.0);
    //float sigma2_n = N0_Hz*SampleRate; // puissance de bruit thermique ajouté
    
    
    float Ge_dB = 10.0;
    float Gr_dB = 10.0;
    float Fc = 1240e6;
    float alpha = 2.0;
    
    float Ge = pow(10.0,Ge_dB/10.0);
    float Gr = pow(10.0,Gr_dB/10.0);
    
    float cpm_Pe_dBm = 28.0;
    float cpm_Pe = pow(10.0,(cpm_Pe_dBm-30)/10.0);
    float Eh = 0.0;
    float Dist = 0.0;
    float PL = 0.0;
    float Pr_data = 1e-14;
    float Pr_data_dBm = -110.0;
    
    char StrPow[200];
    
    float complex mean_value = 0.0 + _Complex_I *0.0;
    float Pr = 0.0; 
    float Pr_dBm  = 0.0;
    float Pr_FFT = 0.0; 
    float Pr_FFT_dBm  = 0.0;
    
    // QPSK symbol tracking
    int num_written=0;
    // symtrack_cccf_reset(symtrack); 
    int Splash=0;
    int NumFrame=0;
    int nRead=0;
    while(1)
    {
        NumFrame++;
        
        nRead=fread(iqin_i16,sizeof(i16cmplx),nFFT,input_fid);
        if(nRead<=0) break;
        for(int i=0;i<nRead;i++)
        {
          iqin[i]=(iqin_i16[i].re /32767.0) + _Complex_I *(iqin_i16[i].im /32767.0);
          iirfilt_crcf_execute(dc_blocker, iqin[i], &iqin[i]);
        }
        
        
       
        if((NumFrame%ShowFrame)==0)
        {
           mean_value = 0.0 + _Complex_I *0.0;;
           for(int i=0;i<nFFT;i++){mean_value = mean_value + iqin[i];} 
           mean_value = mean_value/(float)nFFT;
        
           
           
           // =================================================================================================================
          // ======================================= calcul puissance =======================================================
          // =================================================================================================================
          
           Pr = 0.0;
           for(int i=0;i<nFFT;i++)
           {
               iqin[i] = iqin[i] - mean_value;
               Pr = Pr + (cabsf(iqin[i])*cabsf(iqin[i]));
            } 
            Pr = Pr/(float)nFFT;
            Pr_dBm = 10.0*(log(Pr)/log(10.0))+30.0;
            

          // =================================================================================================================
          // =================================================================================================================
          // =================================================================================================================
          
          fft_execute(fft_plan); // FFT : input: iqin  --->  input: fftout
           
           Pr_FFT = 0.0;
         
           for(int i=nFFT/2;i<nFFT;i++)
           {
             PowerFFT_x[i-nFFT/2]=i-nFFT/2+SpectrumPosX;
             
             Pr_FFT = Pr_FFT + (cabsf(fftout[i])*cabsf(fftout[i]));
             dbAmp=20.0*log(cabsf(fftout[i])/nFFT);
             if(dbAmp>dbpeak) dbpeak=dbAmp;
             
             idbAmp=(unsigned int)((dbAmp+FloorDb)*height/2.0/FloorDb*ScaleFFT);
             PowerFFT_y[i-nFFT/2]=idbAmp+SpectrumPosY;
             idbAmpColor=(unsigned int)((dbAmp+FloorDb)*2*ScaleFFT);//(unsigned int)((dbAmp+200));
             
             fftLine[i-WaterfallWidth/2]=SetColorFromInt(idbAmpColor);
             Oscillo_x[i]=OscilloPosX+i*OscilloWidth/nFFT;
             Oscillo_y_re[i]=OscilloPosY+height/4+crealf(iqin[i])*height/4; // ******
             Oscillo_y_im[i]=OscilloPosY+height/4+cimagf(iqin[i])*height/4; // ******

           }

            for(int i=0;i<nFFT/2;i++)
            {
                PowerFFT_x[i+nFFT/2]=i+nFFT/2+SpectrumPosX;
                
                Pr_FFT = Pr_FFT + (cabsf(fftout[i])*cabsf(fftout[i]));
                dbAmp=20.0*log(cabsf(fftout[i])/nFFT);
                if(dbAmp>dbpeak) dbpeak=dbAmp;
                
                idbAmp=(unsigned int)((dbAmp+FloorDb)*height/2.0/FloorDb*ScaleFFT);
                PowerFFT_y[i+nFFT/2]=idbAmp+SpectrumPosY;
                
                idbAmpColor=(unsigned int)((dbAmp+FloorDb)*2*ScaleFFT);//(unsigned int)((dbAmp+200));
                fftLine[i+WaterfallWidth/2]=SetColorFromInt(idbAmpColor);
                
                Oscillo_x[i]=OscilloPosX+i*OscilloWidth/nFFT;
                Oscillo_y_re[i]=OscilloPosY+height/4+crealf(iqin[i])*height/4;
                Oscillo_y_im[i]=OscilloPosY+height/4+cimagf(iqin[i])*height/4;
            }
          
            Pr_FFT = (Pr_FFT/(float)nFFT)/(float)nFFT;
            
            Pr_FFT_dBm = 10.0*(log(Pr_FFT)/log(10.0))+30.0;
            
            Pr_data = Pr_FFT-sigma2_n;
            
            if (Pr_data < 1e-14) Pr_data = 1e-14;
            
            Pr_data_dBm = 10.0*(log(Pr_data)/log(10.0))+30.0;
            
            Eh = Pr_data/cpm_Pe;
            PL = -10.0*(log(Eh)/log(10.0));
            
            Dist = pow(Ge*Gr/Eh,1.0/alpha)*pow(3e8/(4*pi*Fc),2.0/alpha);
            
            sprintf(StrPow,"Rx sig pow = %2.3f [dBm]  --  Rx data pow = %2.3f [dBm]  --  Path_loss = %2.3f [dB]  --  dist = %2.3f [m].",Pr_FFT_dBm,Pr_data_dBm,PL,Dist);
            
          // =================================================================================================================
          // ======================================= affichage courbes =======================================================
          // =================================================================================================================
          ClipRect(OscilloPosX, height/2, width-nFFT, height/2);
          WindowClear();  
          ClipEnd();
          
          
          
          //ClipRect(0, height/2,nFFT, height/2);
          Stroke(0, 128, 192, 0.8);   
          Polyline(PowerFFT_x, PowerFFT_y, nFFT); // ******************** frequency plot (abs part)
          //vgCopyPixels(0,height/2,0,height/2+RemanenceRate,nFFT,height/2-RemanenceRate);  
          //ClipEnd();
          
          
          if(Splash<fps*5)
           {
            //Title  
            Stroke(0, 128, 128, 1);            
            Fill(0,128-Splash,128-Splash, 1);
            Text(nFFT+nFFT/2,height-150, sTitle, SerifTypeface, 20);
            Splash++;
           }
          
           Stroke(0, 128, 128, 1);     
           Fill(0,128,128, 1);       
           Text(nFFT,height-50,StrPow, SerifTypeface, 10 ); // StrPow 

          
          
          Polyline(Oscillo_x, Oscillo_y_re, nFFT);  // ******************** time plot (real part)
          Stroke(0, 128, 0, 1);
          Polyline(Oscillo_x, Oscillo_y_im, nFFT);  // ******************** time plot (imaginay part)
          
          makeimage(WaterfallPosX,height/2-1,WaterfallWidth,1,(VGubyte *)fftLine); // ******************** Waterfall plot
          //vgCopyPixels(0,0,0,1,nFFT,height/2);
          
          
          // =================================================================================================================
          // =========================================== Constellation =======================================================
          // =================================================================================================================
          memset(fftImageConstellation,0,ConstellationWidth*(height/2)*sizeof(unsigned int)); // Constellation is black : Fixme ; need a window of I/Q constell for summing and averaging

          symtrack_cccf_execute_block(symtrack,iqin,nFFT,derot_out, &num_written); // ****** constellation plot
          
          
          for(int i=0;i<num_written;i++)
          {
            PointY=(cimagf(derot_out[i])+1.5)/3.0*height/2;
            PointX=(crealf(derot_out[i])+1.5)/3.0*ConstellationWidth;
            Point=PointX+ConstellationWidth*PointY;
            if((Point>0)&&(Point<ConstellationWidth*height/2)) 
                fftImageConstellation[Point]=SetColorFromFloat(1.0);
           }    
           makeimage(ConstellationPosX,ConstellationPosY,ConstellationWidth,height/2,(VGubyte *)fftImageConstellation);
           // =================================================================================================================
          // =================================================================================================================
          // =================================================================================================================
           
           End();
       }
       
       
       
    }// end while
    fclose (input_fid);
    End();						   // End the picture
    free(PowerFFT_x);
    free(PowerFFT_y);    
    fft_destroy_plan(fft_plan);
    free(iqin);
    free(fftout);
    free(fftImage);
    finish();
    exit(0);
}

