clear all
close all
clc
% ===========================
cpm_M = 2;
nOFDMSymbols = 8;
nFFT = 128;
% ===========================
nbBitPerSymbol = log2(cpm_M);
nCodedSubFrameBits = nOFDMSymbols*nFFT*nbBitPerSymbol;

Permut = int16(randperm(nCodedSubFrameBits).');
% ===========================
FileName='Permut_CPFSK_8_128.bin';    
fileID = fopen(FileName,'w'); 
count = fwrite(fileID,Permut,'int16');         
if (count~=nCodedSubFrameBits)
    warning('Erreur decriture dans le fichier.')
end
% ===========================
