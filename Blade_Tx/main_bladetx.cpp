
#include <iostream>
//#include <chrono>

#include <math.h>
#include <string.h>
#include <getopt.h>
#include <termios.h>
#include <ctype.h>
#include <unistd.h> // usleep
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include "libbladeRF.h"
#include "bladeRF1.h"
#include "bladeRF2.h"

#define pi 3.141592653589793

using namespace std;

#define EXAMPLE_SAMPLERATE 2000000
#define EXAMPLE_BANDWIDTH BLADERF_BANDWIDTH_MIN

#define EXAMPLE_RX_FREQ 910000000
#define EXAMPLE_RX_GAIN 26

#define EXAMPLE_TX_FREQ 920000000
#define EXAMPLE_TX_GAIN (-20)


#define FPGA_FNAME_115 "./hostedx115.rbf"
#define FPGA_FNAME_40 "./hostedx40.rbf"
#define FPGA_FNAME_A4 "./hostedxA4.rbf"
#define FPGA_FNAME_A9 "./hostedxA9.rbf"

#define PROGRAM_VERSION "0.0.1"

bladerf_channel  channel = BLADERF_CHANNEL_TX(0);
static bool keep_running= true;
//Device structure, should be initialize to NULL
struct bladerf *m_dev= NULL;

FILE *input_fid;


unsigned int m_Bandwidth =  55000000;

#define BUFFER_SIZE 10000 //(1024*500)


typedef struct {
	        int16_t re;
	        int16_t im;
            }i16cmplx;

int finish()
{
    if (m_dev != NULL)
    {
	//Disable TX channel
	int status = bladerf_enable_module(m_dev,channel, false);
	if (status != 0) 
	{
	    fprintf(stderr, "Failed to disable TX: %s\n", bladerf_strerror(status));
	}
	// Close device
	bladerf_close(m_dev);
	fprintf(stderr,"[SDR TX] BladeRF is Closed with failure.\n");
	m_dev = NULL;
    } 
    exit(-1);
}	    
	    
static void signal_handler(int signo)
{
    if (signo == SIGINT)
        fputs("\n[SDR TX] Caught SIGINT\n", stderr);
    else if (signo == SIGTERM)
        fputs("\n[SDR TX] Caught SIGTERM\n", stderr);
    else if (signo == SIGHUP)
        fputs("\n[SDR TX] Caught SIGHUP\n", stderr);
    else if (signo == SIGPIPE)
        fputs("\n[SDR TX] Received SIGPIPE.\n", stderr);
    else
        fprintf(stderr, "\nCaught signal: %d\n", signo);

    keep_running = false;
}



void print_usage()
{

	fprintf(stderr, \
		"bladetx -%s\n\
Usage:\nlimetx -s SymbolRate [-i File Input] [-o File Output] [-f Frequency in Khz]  [-g Gain] [-t SampleType] [-h] \n\
-i            Input IQ File I16 format (default stdin) \n\
-s            SampleRate in KS \n\
-f            Frequency in Mhz\n\
-g            Gain 0 to 100\n\
-h            help (print this help).\n\
Example : ./limetx -sr 1000 -f 1242000 -g 80\n\
\n", \
PROGRAM_VERSION);

} /* end function print_usage */
    
int main(int argc, char *argv[]) 
{
	bool repeat_sending = true;	
	
	
	const char *SerialNumber = "1D4C3A8E0456D0";
	input_fid = stdin;
	
	unsigned int SampleRate = 0;
        uint64_t CenterFrequency = 0;
        int Gain = -1;
      
	// *************************************************************************************
	int a;
	int anyargs = 0;
	     
	    
	while (1)
	{
		a = getopt(argc, argv, "i:s:f:g:h:");

		if (a == -1)
		{
		    if (anyargs) break;
		    else a = 'h'; //print usage and exit
		}
		anyargs = 1;

		switch (a)
		{
		case 'i': // InputFile
			if ( optarg != NULL ) 
			{
			    input_fid = fopen(optarg, "rb");
			    if ( input_fid == NULL )
			    {
				    fprintf(stderr, "[SDR TX] Unable to open '%s': %s\n",optarg, strerror(errno));
				    exit(EXIT_FAILURE);
			    }
			}
			break;
		case 's': // SymbolRate in KS
			SampleRate = atol(optarg)*1000;
			break;
		case 'f': // TxFrequency in Mhz
			CenterFrequency = atol(optarg)*1000000;
			break;
		case 'g': // Gain 0..100
			Gain = atoi(optarg);
			break;
		case 'h': // help
			print_usage();
			exit(0);
			break;
		case -1:
			break;
		case '?':
			if (isprint(optopt))
			{
			   fprintf(stderr, "[SDR TX] limetx `-%c'.\n", optopt);
			}
			else
			{
			   fprintf(stderr, "[SDR TX] limetx: unknown option character `\\x%x'.\n", optopt);
			}
			print_usage();

			exit(1);
			break;
		default:
			print_usage();
			exit(1);
			break;
	}/* end switch a */
	}/* end while getopt() */   

	
        // *************************************************************************************
        bool isapipe = (fseek(input_fid, 0, SEEK_CUR) < 0); //Dirty trick to see if it is a pipe or not
        if (isapipe)
	   fprintf(stderr, "[SDR TX] Using IQ live mode\n");
        else
	   fprintf(stderr, "[SDR TX] Read from data File\n");
        
	if (CenterFrequency == 0) {fprintf(stderr, "[SDR TX] Need set a frequency to tx\n"); exit(0);}
        if (SampleRate == 0) {fprintf(stderr, "[SDR TX] Need set a SampleRate \n"); exit(0);}
        if (Gain == -1) {fprintf(stderr, "[SDR TX] Need set a Gain \n"); exit(0);}
	
	m_Bandwidth = (SampleRate < BLADERF_BANDWIDTH_MIN) ?  BLADERF_BANDWIDTH_MIN:SampleRate;
	
        // register signal handlers		
	if (signal(SIGINT, signal_handler) == SIG_ERR)
		fputs("[SDR TX] Warning: Can not install signal handler for SIGINT\n", stderr);
	if (signal(SIGTERM, signal_handler) == SIG_ERR)
		fputs("[SDR TX] Warning: Can not install signal handler for SIGTERM\n", stderr);
	if (signal(SIGHUP, signal_handler) == SIG_ERR)
		fputs("[SDR TX] Warning: Can not install signal handler for SIGHUP\n", stderr);
	if (signal(SIGPIPE, signal_handler) == SIG_ERR)
		fputs("[SDR TX] Warning: Can not install signal handler for SIGPIPE\n", stderr);
		
	// =======================================================================================================================================================================================================
	// ########################################################################################################################################################################################################
	// =======================================================================================================================================================================================================
        int status;
	

        // Open device
	
	status = bladerf_open(&m_dev,"*");
	if (status != 0) {
	       fprintf(stderr, "[SDR TX] Failed to open device: %s\n",bladerf_strerror(status));
	       finish();
	}
	
	// Open device with serial number
	
	//struct bladerf_devinfo info;

	// /* Initialize all fields to "don't care" wildcard values.
	//*
	//* Immediately passing this to bladerf_open_with_devinfo() would cause
	//* libbladeRF to open any device on any available backend. */
	//bladerf_init_devinfo(&info);

	// /* Specify the desired device's serial number, while leaving all other
	//* fields in the info structure wildcard values */
	//strncpy(info.serial, serial, BLADERF_SERIAL_LENGTH - 1);
	//info.serial[BLADERF_SERIAL_LENGTH - 1] = '\0';

	//status = bladerf_open_with_devinfo(&m_dev, &info);
	//if (status == BLADERF_ERR_NODEV) {
		//printf("No devices available with serial=%s\n", serial);
		//return NULL;
	//} else if (status != 0) {
		//fprintf(stderr, "Failed to open device with serial=%s (%s)\n", serial,
			//bladerf_strerror(status));
		//return NULL;
	//} else {
		//return m_dev;
	//}
	
	
	
	  // Figure out FPGA size 
	  bladerf_fpga_size fpga_size ;
	  status = bladerf_get_fpga_size(m_dev, &fpga_size ) ;
	  if ( status < 0) {
	     fprintf( stderr, "[SDR TX] Couldn't determine FPGA size: %s\n", bladerf_strerror(status) ) ;
	     finish();
	  }
	  
	  // Load the FPGA 
	  if ( fpga_size == BLADERF_FPGA_40KLE ) 
		fprintf( stderr, "[SDR TX] FPGA_FNAME_40 \n") ;
	  else if ( fpga_size == BLADERF_FPGA_115KLE ) 
		fprintf( stderr, "[SDR TX] FPGA_FNAME_115 \n") ;
	  else if ( fpga_size == BLADERF_FPGA_A4 )
		fprintf( stderr, "[SDR TX] FPGA_FNAME_A4 \n") ;
	  else if ( fpga_size == BLADERF_FPGA_A9 )
		fprintf( stderr, "[SDR TX] FPGA_FNAME_A9 \n") ;
	  else {
	        fprintf( stderr, "[SDR TX] Incompatible FPGA size.\n") ;
	    }
	  
		
	  // Load the FPGA 
	  //if ( fpga_size == BLADERF_FPGA_40KLE ) 
	        //status = bladerf_load_fpga(m_dev, FPGA_FNAME_40) ;
	  //else if ( fpga_size == BLADERF_FPGA_115KLE ) 
	        //status = bladerf_load_fpga(m_dev, FPGA_FNAME_115) ;
	  //else if ( fpga_size == BLADERF_FPGA_A4 )
	        //status = bladerf_load_fpga(m_dev, FPGA_FNAME_A4) ;
	  //else if ( fpga_size == BLADERF_FPGA_A9 )
	        //status = bladerf_load_fpga(m_dev, FPGA_FNAME_A9) ;
	  //else {
	        //fprintf( stderr, "[SDR TX] Incompatible FPGA size.\n") ;
	        //finish();
	    //}
	    
	  //if (status < 0 ) 
	  //{
	        //fprintf( stderr, "[SDR TX] Couldn't load FPGA: %s\n", bladerf_strerror(status) ) ;
	        //finish();
	   //}
	
	
	//CenterFrequency
	status = bladerf_set_frequency(m_dev, channel, CenterFrequency);
	if (status != 0)
	{
	    fprintf(stderr, "[SDR TX] Failed to set frequency = %u: %s\n",CenterFrequency, bladerf_strerror(status));
	    finish();
	}
	
	//Set sample rate
	status = bladerf_set_sample_rate(m_dev, channel,SampleRate, NULL);
	if (status != 0)
	{
	    fprintf(stderr, "[SDR TX] Failed to set samplerate = %u: %s\n",SampleRate, bladerf_strerror(status));
	    finish();
	}
	
	// Set alalog LPF filter bandwidth
	status = bladerf_set_bandwidth(m_dev, channel,m_Bandwidth, NULL);
	if ( status != 0) 
	{
	    fprintf(stderr, "[SDR TX] Failed to set bandwidth = %u: %s\n",m_Bandwidth, bladerf_strerror(status));
	    finish();
	}
	
	// Set TX channel gain
	status = bladerf_set_gain(m_dev,channel,Gain);
	if (status != 0) 
	{
	    fprintf(stderr, "[SDR TX] Failed to set TX channel gain: %s\n",bladerf_strerror(status));
	    finish();
	}

       // =======================================================================================================================================================================================================
       // ########################################################################################################################################################################################################
       // =======================================================================================================================================================================================================  
	uint64_t freq;
        unsigned int bw;
        struct bladerf_rational_rate rate;
	int ggain;
    
	status = bladerf_get_frequency(m_dev,channel,&freq);
	status = bladerf_get_bandwidth(m_dev,channel, &bw);
	status = bladerf_get_rational_sample_rate(m_dev,channel, &rate);	
	status = bladerf_get_gain(m_dev,channel,&ggain);
	
	fprintf(stderr,"  [SDR TX] RX Gain: in = %d  --  out= %d .\n", Gain,ggain);
	fprintf(stderr,"  [SDR TX] RX LO Frequency: in = %2.3f [MHz] --  out= %2.3f [MHz].\n",CenterFrequency/1e6,freq/1e6);
	fprintf(stderr,"  [SDR TX] RX LPF Bandwidth: in = %2.3f [MHz] --  out= %2.3f [MHz].\n", m_Bandwidth/1e6,bw/1e6);
	fprintf(stderr,"  [SDR TX] RX SampleRate: in = %2.3f [KHz] --  out= %2.3f [KHz].\n", SampleRate/1e3,rate.integer/1e3);
       // =======================================================================================================================================================================================================
       // ########################################################################################################################################################################################################
       // =======================================================================================================================================================================================================  
	
	// configure TX stream
	bladerf_channel_layout layout = BLADERF_TX_X1; /* BLADERF_TX_X1  |  BLADERF_TX_X2  */ 
	bladerf_format         dataFmt = BLADERF_FORMAT_SC16_Q11; /* BLADERF_FORMAT_SC16_Q11_META  |  BLADERF_FORMAT_SC16_Q11  */ 	
	const unsigned int     num_buffers   = 16; // 256
        const unsigned int     buffer_size   = 1024*2; // 1024
        const unsigned int     num_transfers = 8; // 32
        const unsigned int     timeout_ms    = 5000; //4000

	status = bladerf_sync_config(m_dev,layout,dataFmt,num_buffers,buffer_size,num_transfers,timeout_ms);
	if ( status < 0)    
	{
	    fprintf(stderr, "[SDR TX] Couldn't configure TX stream: %s\n", bladerf_strerror(status) ) ;
	    finish();
	}
	else fprintf(stderr, "[SDR TX] bladerf_sync_config is valid (status = %i).\n",status) ;
	
	// Enable TX module
	status = bladerf_enable_module(m_dev,channel, true);
	if ( status < 0) 
	{
	    fprintf(stderr, "[SDR TX] Couldn't enable TX module: %s\n", bladerf_strerror(status) ) ;
	    finish();
	}
	else fprintf(stderr, "[SDR TX] bladerf_enable_module is valid (status = %i).\n",status) ;
	
	// ==========================================================================================
	// Configure TX
	// ==========================================================================================
		
	struct bladerf_metadata tx_metadata;
	//memset(&tx_metadata, 0, sizeof(tx_metadata));
	tx_metadata.timestamp = 0;
	tx_metadata.status = 0;
        //tx_metadata.flags = BLADERF_META_FLAG_TX_BURST_END; // BLADERF_META_FLAG_TX_BURST_START | BLADERF_META_FLAG_TX_NOW | BLADERF_META_FLAG_TX_BURST_END;

	// ========
	int nRead;
	i16cmplx tx_buffer[BUFFER_SIZE];    
	
	int16_t *samples = (int16_t *) malloc(BUFFER_SIZE * 2 * sizeof(int16_t));
	    if (samples == NULL) {
		perror("malloc");
		finish();
	    } 
	
	// **************************************************************************************************************	
	// Set TX channel gain to zero
	status = bladerf_set_gain(m_dev,channel,0);
	if (status != 0) 
	{
	    fprintf(stderr, "[SDR TX] Failed to set TX channel gain: %s\n",bladerf_strerror(status));
	    finish();
	}
	// **************************************************************************************************************

         bool Transition=true;
        int TotalSampleSent=0;
        static int debugCnt = 0;
	
	if ((status == 0) && keep_running)
	   fprintf(stderr,">>>> [SDR RX] starting data sent ......\n");
	else
	   fprintf(stderr,">>>> [SDR RX] Problem of starting transmit data ......\n");
	
        while ((status == 0) && keep_running)
        { 
	    // Fill Tx Buffer
	    nRead = fread(tx_buffer,sizeof(i16cmplx),BUFFER_SIZE,input_fid);
		
	    if (nRead < BUFFER_SIZE)
	    { 
		if (nRead > 0) fprintf(stderr,"[SDR TX] Incomplete buffer %d/%d\n", nRead, BUFFER_SIZE);
		else	
		{	 
		    if (isapipe) break;
		    else
		    {
		       if (repeat_sending)  
		       {
			     fseek(input_fid,0, SEEK_SET);
			     continue;
		       }
		       else 
		       break;
		    }
		}
	    }
	    
	    
	    
	    for (int i = 0; i < nRead; i ++) {
		samples[2*i]     = (int16_t) floor(tx_buffer[i].re*(2047.0/32767.0));  
		samples[2*i + 1] = (int16_t) floor(tx_buffer[i].im*(2047.0/32767.0));  		
	    }
	    
	    // Send samples
	    status = bladerf_sync_tx(m_dev,samples,nRead,nullptr,1000); // &tx_metadata
	    if (status != 0) fprintf(stderr, "TX failed: %s\n", bladerf_strerror(status));
	    
	    TotalSampleSent+=nRead;
	    tx_metadata.timestamp += nRead;
	    
	    // **************************************************************************************************************
	    if(Transition)
	    {
		if(TotalSampleSent>SampleRate) // 1 second
		{
		   // Set TX channel gain
		   status = bladerf_set_gain(m_dev,channel,Gain);
		   if (status != 0) 
		   {
			fprintf(stderr, "[SDR TX] Failed to set TX channel gain: %s\n",bladerf_strerror(status));
			finish();
		   }
			
		   Transition=false;		
		}
	    }
	    // **************************************************************************************************************
	    debugCnt++ ;
	    
	} // End While
	
	// close binary file
        fclose(input_fid); 
	fprintf(stderr,"[SDR TX] Input File is Closed with success.\n");
	
	
	//Disable TX channel
	status = bladerf_enable_module(m_dev,channel, false);
	if (status != 0) 
	{
	    fprintf(stderr, "Failed to disable TX: %s\n", bladerf_strerror(status));
	}
	// Close device
	bladerf_close(m_dev);
	fprintf(stderr,"[SDR TX] BladeRF is Closed with success.\n");
	
	m_dev = NULL;
	return 0;
	
}
